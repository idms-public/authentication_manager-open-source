# Authentication Manager
## Table of Contents
1. [Useful Commands](#spreg)
2. [Open Source Docs](#open-source)
  * [Docker Setup](#open-dock)
  * [Database Setup](#open-dock)
  * [Application Setup](#open-dock)
3. [Duke Docs](#duke)



## Useful Commands
note(see initial configuration if this is a fresh install)
- `docker ps -a` show all running docker containers
- `docker-compose up -d` create docker containers based off of a `docker-compose.yml` the `-d` flag runs containers in a detached state
- `docker-compose down` destroy docker containers based off of a `docker-compose.yml`
- `docker-compose stop` stop docker containers based off of a `docker-compose.yml`
- `docker-compose start` start docker containers based off of a `docker-compose.yml`

**note** you typically don't want to destroy containers, rather stop and start them as needed

- `docker-compose exec auth-manager rails c` opens up a rails console
- `docker-compose exec auth-manager rake` runs the test suite
- `docker-compose exec auth-manager bash` opens a bash console on the webapp container
- `docker attach auth-manager` enables you to see console output from the container

# Open Source Documentation <a name="open-source"></a>
## Setup
### 1. Build docker images and start containers. <a name="open-dock"></a>
  **NOTE: run all commands from root of repository**
  - if not already installed be install both `docker` and `docker-compose`  https://docs.docker.com/engine/installation/ and https://docs.docker.com/compose/install/
  - run `docker-compose build` This will create 4 docker images
    - auth-manager (web ui, localhost:3000)
    - postgress (db for dev)
    - pgadmin4 (db admin web ui for localhost:4000)
    - spreg (backend)
  - run `docker-compose build` you will only need to do this the first time and anytime you modify the Gemfile
  - run `docker-compose up -d; docker attach` this will spin up a container for authentication manager on localhost:3000
  - run `docker ps -a` and you should see the 4 containers running

### 2. Configure Database <a name="open-db"></a>
  - run `docker-compose exec auth-manager bash` This will create a bash session inside the auth-manager container.
  - Inside the bash session run the following commands in order:
    - `rails db:create` - creates a dev and test db in the postgres container
    - `rails db:migrate` - migrates tables to db
    - `rails db:seed` - sets up db with mock values, this can be skipped if desired

### 3. Configure Application <a name="open-app"></a>

# Duke Documentation <a name="duke"></a>
## Initial Configuration
### 1. Running App in Docker
 - from the root off the project run `scp netid@idms-web-netid-01.oit.duke.edu:/srv/docker_idms/idms-web-netid/scripts/idms_properties.yml config/idms_properties.yml`
 - be sure to have both `docker` and `docker-compose` installed https://docs.docker.com/engine/installation/ and https://docs.docker.com/compose/install/
 - from the root of the app, run `docker-compose build` you will only need to do this the first time and anytime you modify the Gemfile
 - run `docker-compose up -d; docker attach` this will spin up a container for authentication manager on localhost:3000
 - run `docker-compose stop` to stop the containers and `docker-compose start` to start them back up
 - to access a rails console you will need to run `docker-compose exec auth-manager rails console`


## 2. Troubleshooting
 - You will need to have a copy of the Gemfile and Gemfile.lock in order for the image to build
 - You must be connected to the oit-ssi vpn to run the app.
 - "Could not fetch specs from https://gems-internal.oit.duke.edu/" you may need to connect to the vpn in order for bundle to work.
 - "Docker container will not start" delete anything inside of tmp/pids



# Deployment
### Production Deployment
1. push changes upstream to gitlab and merge into master branch
2. ssh to idms-web-netid-01.oit.duke.edu and idms-web-netid-02.oit.duke.edu
3. Run the following command: `/srv/docker_idms/idms-web-authn/scripts/quick_deploy_manager.sh`

### Test Deployment
1. push changes upstream to gitlab and merge into test branch
2. ssh to idms-web-netid-test-01.oit.duke.edu
3. Run the following command: `/srv/docker_idms/idms-web-authn-test/scripts/quick_deploy_manager.sh`


## SPREG <a name="spreg"></a>
### 1. Get Dev IDP files
* In your local project `cd` into `vendor/shibboleth`
* if you do not already have the repos run `svn co svn+ssh://da129@community.oit.duke.edu/srv/svn/idms/shibboleth/authnmgr-test/metadata` and `svn co svn+ssh://da129@community.oit.duke.edu/srv/svn/idms/shibboleth/authnmgr-test/conf`
* other wise `cd` into `vendor/shibboleth/metadata` and `vendor/shibboleth/conf` and run `svn up`

## 2. Running the SPREG backend(test)
**where** idms-admin-dev-04:/srv/idms/applications/spreg/bin

**how:**  `sudo ./start.sh`

**logs** /srv/idms/logs/applications/spreg/

## 3. getting shib files for dev

shib-idp-09.oit.duke.edu:

grab /srv/shibboleth-idp/metadata/local-sites.xml and put it here vendor/shibboleth/metadata/local-sites.xml


shib-idp-09.oit.duke.edu:

grab /srv/shibboleth-idp/conf and put it here vendor/shibboleth/conf/attribute-filter.xml
#@
#end
