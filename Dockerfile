FROM ruby:2.3.6

### System Libraries
RUN apt-get update -qq && apt-get install -y build-essential nodejs
RUN apt-get update && apt-get install -y --no-install-recommends apt-utils
RUN apt-get install -y zip unzip
RUN apt-get install -y libpq-dev



### Install Oracle Instant Client ###
RUN apt-get install libaio1
RUN mkdir -p /opt/oracle

WORKDIR /opt/oracle
ADD vendor/oracle/instantclient-basic-linux.x64-12.1.0.2.0.zip /opt/oracle/instantclient-basic-linux.x64-12.1.0.2.0.zip
ADD vendor/oracle/instantclient-sdk-linux.x64-12.1.0.1.0.zip /opt/oracle/instantclient-sdk-linux.x64-12.1.0.1.0.zip
ADD vendor/oracle/instantclient-sqlplus-linux.x64-12.1.0.1.0.zip /opt/oracle/instantclient-sqlplus-linux.x64-12.1.0.1.0.zip

RUN unzip instantclient-basic-linux.x64-12.1.0.2.0.zip
RUN unzip instantclient-sdk-linux.x64-12.1.0.1.0.zip
RUN unzip instantclient-sqlplus-linux.x64-12.1.0.1.0.zip

RUN cd /opt/oracle/instantclient_12_1; ln -s libclntsh.so.12.1 libclntsh.so
ENV LD_LIBRARY_PATH="/opt/oracle/instantclient_12_1"
#RUN echo "export LD_LIBRARY_PATH=/usr/lib/oracle/12.1/client64/lib:$LD_LIBRARY_PATH" >> /root/.bashrc


WORKDIR /rails-app
ADD Gemfile.lock .
ADD Gemfile .
RUN gem install bundler && bundle install

ADD . ./

EXPOSE 3000
