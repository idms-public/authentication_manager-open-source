require_relative 'boot'

require 'rails/all'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module AuthenticationManager
  class Application < Rails::Application
    config.load_defaults 5.1

    #part of the rails 5 upgrade
    ActiveSupport.halt_callback_chains_on_return_false = false

    config.time_zone = 'Eastern Time (US & Canada)'
    #TODO configureable oci8 settings
    # OCI8::BindType::Mapping[Time] = OCI8::BindType::LocalTime
    # OCI8::BindType::Mapping[:date] = OCI8::BindType::LocalTime
    # OCI8::BindType::Mapping[Time] = OCI8::BindType::LocalTime

  end
end
