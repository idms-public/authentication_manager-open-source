module AuthSecrets
	def self.values
		if Rails.application.secrets.use_auth_secrets
			secret_conf  = YAML.load_file('config/idms_properties.yml')["oim"]
			iv = secret_conf['iv']
			key = secret_conf['key']
			conn = IdmsSecrets::Connection.new(env: Rails.env, pass: secret_conf["pass"])



			secrets = {}
			env = Rails.env

			secrets['idm_ws_admin'] = conn.get_secret(secret: 'idm.ws.admin.username',iv: iv, key: key)
			secrets['idm_ws_pass'] = conn.get_secret(secret: 'idm.ws.admin.password',iv: iv, key: key)

			secrets['idmanager_user'] = conn.get_secret(secret: 'idmgr.user',iv: iv, key: key)
			secrets['idmanager_user_id'] = conn.get_secret(secret: 'idmgr.id',iv: iv, key: key)
			secrets['idmanager_pass'] = conn.get_secret(secret: 'idmgr.pass',iv: iv, key: key)

			secrets['svcdir_providerUrl'] = conn.get_secret(secret: 'ldap.svcdir.providerUrl', iv: iv, key: key)
			secrets['svcdir_bindDn'] = conn.get_secret(secret: 'ldap.svcdir.bindDn', iv: iv, key: key)
			secrets['svcdir_password'] = conn.get_secret(secret: 'ldap.svcdir.password', iv: iv, key: key)

			#spreg database
			secrets['spreg_db_host'] = conn.get_secret(secret: "db.spreg.host",iv: iv, key: key)
			secrets['spreg_db_username'] = conn.get_secret(secret: "db.spreg.username",iv: iv, key: key)
			secrets['spreg_db_password'] = conn.get_secret(secret: "db.spreg.password",iv: iv, key: key)

			#todo need to put in prod secrets
			if env != "production"
				#auth manager database
				secrets['auth_db_username'] = conn.get_secret(secret: "db.authmanager-dev.username",iv: iv, key: key)
				secrets['auth_db_password'] = conn.get_secret(secret: "db.authmanager-dev.password",iv: iv, key: key)
				secrets['auth_db_url'] = conn.get_secret(secret: "db.authmanager-dev.url",iv: iv, key: key)
			else
				secrets['auth_db_username'] = conn.get_secret(secret: "db.authmanager.username",iv: iv, key: key)
				secrets['auth_db_password'] = conn.get_secret(secret: "db.authmanager.password",iv: iv, key: key)
				secrets['auth_db_url'] = conn.get_secret(secret: "db.authmanager.url",iv: iv, key: key)
			end
			secrets
		else
			{}
		end
	end
	VALUES = values
end
