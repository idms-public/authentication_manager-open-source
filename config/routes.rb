Rails.application.routes.draw do
  root 'user#landing'
  
  if Rails.env == "development"
    mount SamlCamel::Engine, at: "/saml"
  end


  scope :user do
    get '/progress/:id' => 'user#project_status', as: 'project_status'
    get '/show_sp' => 'user#show_sp', as: "show_user_sp", :constraints => { :entityId => /[0-z\D]+/ }
    post '/create_user_note' => 'user#create_user_note'
    post '/sp/check_cert' => 'registration#check_cert' #check cert from user path
  end


  scope :register do
    get '/register' => 'registration#new'
    get '/sp' => 'registration#new_sp', as: 'new_sp'

  	post 'register/create_registration' => 'registration#create'
    post '/sp/check_cert' => 'registration#check_cert' #check csp cert
  	post '/edit_sp' => 'registration#edit_sp', as: 'edit_sp'
  	post '/sp/submit' => "registration#submit_new_sp", as: 'submit_sp'

  	delete '/delete_sp' => 'registration#delete_sp', as: 'delete_sp', :constraints => { :entityId => /[0-z\D]+/ }
  end


  scope :admin do
  	get '/' => 'admin#index', as: 'admin_root'
  	get '/open/(:org)' => 'admin#index', as: 'admin_open_request'
  	get '/closed/(:org)' => 'admin#closed', as: 'admin_closed_request'
  	get '/on_hold/(:org)' => 'admin#on_hold', as: 'admin_hold_request'
    get '/details/:id' => 'admin#show', as: 'integration_request'
    get '/statistics' => 'admin#statistics'

  	post '/search/projects' => 'admin#search_projects', as: 'search_projects'
  	post '/set_filter' => 'admin#set_active_filter', as: 'set_active_filter'
  	post '/create_note' => 'admin#create_note'
  	post '/assign_analyst/:id' => 'admin#assign_analyst', as: 'assign_analyst'

  	patch '/update_status/:id' => 'admin#update_status', as: 'component_status'
  	patch '/assign_institution/:id' => 'admin#assign_institution', as: 'assign_institution'
  	patch '/update_note/:id' => 'admin#update_note'

  	delete '/delete_note' => 'admin#delete_note', as: 'work_note'
  end


	scope :spreg do
		get '/' => 'admin/spreg#index', as: 'spreg'
    get '/spreg/search/user' => 'admin/spreg#index', as: "spreg_index"
		get 'show' => "admin/spreg#show", as: "show_sp", :constraints => { :entityId => /[0-z\D]+/ } #allows urls as param
		get 'sps' => "admin/spreg#sp_list"
		post "search" => "admin/spreg#search", as: "search_sp"
    post "search/user" => "admin/spreg#search_by_user", as: "search_by_user"
    post 'attribute_approval' => 'admin/spreg#attribute_approval', as: 'attribute_approval'

    #validate sp cert
    post '/show/sp/check_cert' => 'registration#check_cert'
  	post '/sp/check_cert' => 'registration#check_cert'

		put 'update/:entityId' => "admin/spreg#update", as: "update_sp", :constraints => { :entityId => /[0-z\D]+/ } #allows urls as param
	end


  #other
  get "/docs" => 'documentation#index', as: "docs"
  get "/docs/boxdoc" => 'documentation#box_doc', as: "box_docs"
  get "/logout" => 'application#logout', as: "logout"
  post '/mock_id' => 'application#select_mock_id', as: "mock_id"
  post '/stop_mock' => 'application#stop_mock', as: "stop_mock"
  post '/api' => 'admin/api#create'
  post '/authtest' => 'admin/api#authtest'
  # get '/faq' => 'documentation#faq'
end
