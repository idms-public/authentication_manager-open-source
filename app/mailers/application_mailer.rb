class ApplicationMailer < ActionMailer::Base
  default from: "no-reply@duke.edu"
  layout 'mailer'
end
