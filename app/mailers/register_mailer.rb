class RegisterMailer < ApplicationMailer
	def send_test_email
		mail(to: "danai.adkisson@duke.edu", subject: "Test Email")
	end

	def new_registration(project)
		@project = project
			if Rails.env == "development"
				mail(to: "danai.adkisson@duke.edu",subject: "Shib Integration Request for #{@project.sponsor}")
			else
				mail(to: ["danai.adkisson@duke.edu","lenore.ramm@duke.edu","mary.mckee@duke.edu"], subject: "Shib Integration Request for #{@project.sponsor}")
			end
	end

end
