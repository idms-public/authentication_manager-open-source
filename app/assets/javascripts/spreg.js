function myCallback(result) {
  var cert = $('#cert');
  if(result){
    $('#cert-msg-con').remove();

    cert.css('background-color', '#2AC95C');
    cert.after(
      "<div id='cert-msg-con'>" +
      "<p class='valid-cert-msg'>Valid Certificate</p>" +
      "<p class='valid-cert-msg'>Subject: " + result["subject"] +"</p>" +
      "<p class='valid-cert-msg'>Issuer: " + result["issuer"] +"</p>" +
      "<p class='valid-cert-msg'>Not Before: " + result["before"] +"</p>" +
      "<p class='valid-cert-msg'>Not After: " + result["after"] +"</p>" +
      "</div>"
    );
  } else{
    $('#cert-msg-con').remove();
    cert.css('background-color', '#CC3300');
    cert.after("<div id='cert-msg-con'><p id='invalid-cert-msg'>Invalid Certificate</p></div>")
  }
}

// function validateCert(callback){
function validateCert(callback){
  var certificate = $('#cert').val();
  $.ajax({
    method: "POST",
    url: 'sp/check_cert',
    data: {cert: certificate},
    dataType: "json",

    success: function(response){
      callback(response);
    },
    error: function(req, err){
    }
  });

}


//toggle sp edit fields
function toggleSpFields(editElement){
  $(editElement).nextAll('.sp-show-field').toggle();
  $(editElement).nextAll('.sp-update-field').toggle();

  var updateField = $('.sp-update-field');

  updateField.each(function(){
    if($(this).is(":visible")){
      $(this).addClass("active-submit");
    }else{
      $(this).removeClass("active-submit");
    }
  });

  if(updateField.is(":visible")){
    //note that hide actually gives display none which moves the element out of position
    $('#update-sp-container').removeClass('toggleSpSubmit');
  }else{
    // $('#update-sp-container').hide();
    $('#update-sp-container').addClass('toggleSpSubmit');
  }
}


//add attribute
function addSpAttribute(attrElement){
  var newSpAttr =  $('#new-sp-attr');
  var selectedAttr = newSpAttr.find(":selected").text();

  //check if the field is private and assign the appropriate value
  if(selectedAttr.match('private')){
    var isPublicField = '<input name="attributes[][is_public]" class="long-text-field" id="attributes__is_public" value="false" type="hidden">' +
      '<input name="attributes[][reason]" id="attributes__reason" placeholder="Reason for Attribute?" type="text" style="margin-left: 5px;">';
  }else{
    isPublicField = '<input name="attributes[][is_public]" id="attributes__is_public" class="long-text-field" value="true" type="hidden">';
  }

  //no obvious soution yet, I need the clone to presere the delete behavior, but I do not want the first two to be delete, or even present
  var attrValue = newSpAttr.val();
  // var minusBtn = $(attrElement).next('.sp-dup-set').children('.delete-sp-field');
  var minusBtn = $("#delete-sp-attr-example");
  $(attrElement).parent(".sp-update-field").append(
    '<div class="sp-dup-set"><input name="attributes[][attr]" id="attributes__attr" class="long-text-field" value=' +attrValue +' readonly="readonly" type="text">' + isPublicField +
    ' </div>'

  );
  minusBtn.clone(true,true).appendTo($(attrElement).nextAll(".sp-dup-set").last());
}


//add sp field
function addSpField(spElement){
  var newElement = $(spElement).next('.sp-dup-set').clone(true,true);
  newElement.children('input').val("");
  newElement.children('input').prop('checked', false);
  newElement.val("newValue");
  newElement.appendTo($(spElement).parent('.sp-update-field'));
  // return newElement;
}


//delete sp field
function deleteSpField(spElement){
  $(spElement).parent('.sp-dup-set').remove()
}


//search sp from potential_matches table
function searchPotentalMatches(row){
  var entityID = $(row).children('td').text()
  var form = $("#sp-search-form")
  var input = $("<input>")
               .attr("type", "hidden")
               .attr("name", "entityId").val(entityID);
   form.append($(input));
  // $('#search-potential-sp').val(entityID);
  form.submit();
}


//main
$(document).ready(function(){
  //add sp attr
  $(".add-sp-attr-btn").on('click',function (){
    addSpAttribute(this)
  });

  // duplicate/add spreg field
  $('.dup-spreg-field').on('click',function(){
    addSpField(this)
  });


  //delete spreg field
  //on fields that must have a value, do not delete final value
  $('.delete-sp-field').on('click',function(){
    var oneRequired = $(this).parents('.sp-update-field').hasClass('oneRequired');
    if(oneRequired){
      var itemsLeft = $(this).parents('.sp-update-field').children('.sp-dup-set');
      if(itemsLeft.length !== 1){deleteSpField(this);}
    }else{
      deleteSpField(this);
    }
  });


  //toggle editable fields AND submitbox
  $('#update-sp-container').addClass('toggleSpSubmit');
  $(".sp-update-field").hide();

  $('.sp-edit-icon').on("click",function(){
    toggleSpFields(this)
  });


  //ajax cert check
  $('#cert').on('change',function(){
    validateCert(myCallback);
  });


  //
  $(".potential-sp-row").on('click',function(){
    searchPotentalMatches(this);
  });
});
