//scroll to work note section
$(document).ready(function(){

  //smooth scroll to work notes
  $("#show-work-note").click(function() {
    $('html, body').animate({
      scrollTop: $("#work-note-container").offset().top
    }, 500);
  });

  //when the edit icon is clicked on the work notes page, bring up a form to
  //edit the work note and hide the p tags
  $('.wn-update-form').hide();
  $('.wn-edit-icon').on("click",function(){
    $(this).closest('.wn').find('.wn-update-form').toggle();
    $(this).closest('.wn').find('.wn-note').toggle();
  });
});
