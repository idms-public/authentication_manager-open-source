//= require admin
//= require spreg
//= require validate_sp_form
//= require registration
//= require user


$(document).ready(function(){
  $("#integration-table").tablesorter();
  $("#user-sp-table").tablesorter();
  $("#user-projects-table").tablesorter();



// ----------- Click on table row to nav -----------
  $("div[data-link]").click(function() {
    window.location = $(this).data("link")
  });

  $("tr[data-link]").click(function() {
    window.location = $(this).data("link")
  });

  reg_logic(); //set registration logic
  $('.component-form ').last().addClass("current-form-style");

  //Duplicate component form fields
  questionCounter=1;
  newHtml = $('.component-form').last().html();

  $('.dup-component').click(function(e){
    e.preventDefault();

    var newFields = $('.component-form').last().clone(true,true);
    newHtml = newHtml
      .replace(/\[components_attributes\]\[[0-9]+\]/g, '[components_attributes][' + questionCounter + ']')
      .replace(/components_attributes_[0-9]+_/g, 'components_attributes_' + questionCounter + '_');

    // THIS IS THE GENERIC VERSION OF REPLACE
    // THE PROBLEM IS THAT CONTACTS WERE GETTING ASSIGNED DUPLICATE
    // RAILS IDS
    // .replace(/\[[0-9]+\]/g, '[' + questionCounter + ']')
    // .replace(/_[0-9]+_/g, '_' + questionCounter + '_');
    newFields.html(newHtml);

    $('.component-form').last().after(newFields);

    reg_logic(); //set registration logic for duplicated field
    questionCounter ++

    $('.component-form ').last().addClass("current-form-style");

  });


  // ----------- Pre-Load SPs -----------
  var sps = new Bloodhound({
    datumTokenizer: Bloodhound.tokenizers.whitespace,
    queryTokenizer: Bloodhound.tokenizers.whitespace,
    //ttl is time in milliseconds data should be cached
    prefetch: {
      url: 'spreg/sps',
      ttl: 600000
    }
  });
  // passing in `null` for the `options` arguments will result in the default
  // options being used
  $('#prefetch .typeahead').typeahead(null, {
    name: 'sps',
    source: sps
  });

  $('.show-element').show();
});
