//= require spreg
var pageErrors = "<h3>Error Submitting SP Registration:</h3>";


$(document).ready(function() {
  // $('#spreg-error-container').hide();
  $('#spreg-submit').on('click', function (event) {
    if (checkSpForm()) {
      $(".sp-update-field").each(function () {
        if ($(this).hasClass("active-submit")) {
          handleGroupsAndAttrs($(this));
        } else {
          $(this).remove()
        }
      });
      $('#spreg-form').submit();
    } else {
      event.preventDefault();
      regErrorMsg();
      $('html, body').animate({
        scrollTop: $(this)
      });
    }
  });
});



//if user removes all groups or attributes, append an input to send to SPREG to have values wiped
function handleGroupsAndAttrs(activeSumbitDiv){
  var removeGroups = activeSumbitDiv.hasClass("sp-group-update-fields");
  var removeAttrs = activeSumbitDiv.hasClass("sp-attr-update-fields");
  if(removeGroups){
    if(!activeSumbitDiv.children('.sp-dup-set')[0]){
      activeSumbitDiv.append('<input name="remove_groups" id="groups__path" value="1" placeholder="type in group path here" class="sp-long-field" type="hidden">');
    }
  }else if(removeAttrs){
    if(!activeSumbitDiv.children('.sp-dup-set')[0]){
      activeSumbitDiv.append('<input name=remove_attrs id="attributes__attr" value="1" readonly="readonly" class="long-text-field" type="hidden">')
    }
  }
}


//apply error messages to error div
function regErrorMsg() {
  var errorCon = $('#spreg-error-container');
  errorCon.empty();
  errorCon.show();
  errorCon.append(pageErrors);
  pageErrors = "<h3>Error Submitting SP Registration:</h3>";
}


//generic function to loop through multi value fields
function checkGroupPresence(elements) {
  var complete = true;
  elements.each(function () {
    if($(this).is(':visible')) { //non visable fields are edit fields which are not being submitted
      if ($(this).val() !== "") {
      } else {
        complete = false;
        return false
      }
    }
  });
  return complete;
}



//check contacts netid
function checkContactsNetid() {
  var contacts = $('.sp-contact-netid');
  if (checkGroupPresence(contacts)) {
    return true;
  } else {
    pageErrors = pageErrors + "<p>All Contact fields must have a valid netid.</p>";
    return false;
  }
}



//check contacts email
function checkContactsEmail() {
  var complete = true;
  var emails = $('.sp-contact-email');

  emails.each(function () {
    if ($(this).val().match(/^[^@\.][^@\s]*@{1}[^@\s\.]{1,}\.{1}[^\s@\.][^\s@]*$/) != null) {
      return true;
    } else {
      complete = false;
      pageErrors = pageErrors + "<p>All Contact fields must have a valid email.</p>";
      return false;
    }
  });
  return complete
}




//check that all users have values
function checkUsers() {
  var users = $('.sp-users-netid');
  if (checkGroupPresence(users)) {
    return true;
  } else {
    pageErrors = pageErrors + "<p>All User fields must have a netid.</p>";
    return false;
  }
}


//checks that groups field for edit is filled out with a values
function checkEditGroups(){
  var groups = $('.sp-edit-group-field');
  if (checkGroupPresence(groups)) {
    return true;
  } else {
    pageErrors = pageErrors + "<p>Must provide a value for requested groups.</p>";
    return false;
  }
  return complete;
}

//check that all acs have values
function checkAcs() {
  var acs = $('.sp-acs');
  if (checkGroupPresence(acs)) {
    return true;
  } else {
    pageErrors = pageErrors + "<p>All ACS fields must have a location specified.</p>";
    return false;
  }
}



//check all functions that are single text fields
function validateSingleValueItems() {
  var entityID = ($('#entity_id').val() !== "");
  if (!entityID) {
    pageErrors = pageErrors + "<p>Must specify an Entity ID.</p>";
  }

  var responseibleDept = ($('#responsible_department').val() !== "");
  if (!responseibleDept) {
    pageErrors = pageErrors + "<p>Must specify a Responsible Department.</p>";
  }

  var owningDept = ($('#owning_department').val() !== "");
  if (!owningDept) {
    pageErrors = pageErrors + "<p>Must specify an Owning Department.</p>";
  }

  var purpose = ($('#purpose').val() !== "");
  if (!purpose) {
    pageErrors = pageErrors + "<p>Must briefly explain purpose of this SP.</p>";
  }

  var audience = ($('#audience').val() !== "");
  if (!audience) {
    pageErrors = pageErrors + "<p>Must describe audience of the SP</p>";
  }

  var cert = ($('#cert').val() !== "");
  if (!cert) {
    pageErrors = pageErrors + "<p>Must provide a Certificate for the SP</p>";
  }


  return (entityID && responseibleDept && owningDept && purpose && audience && cert && true);
}


function validateMultivalueItems() {
  var contactsNetid = checkContactsNetid();
  var contactsEmail = checkContactsEmail();
  var users = checkUsers();
  var acs = checkAcs();

  return (contactsNetid && contactsEmail && users && acs && true);
}


function checkSpForm() {
  var checkOne = validateSingleValueItems();
  var checkTwo = validateMultivalueItems();
  var checkGroups = checkEditGroups();

  return (checkOne && checkTwo && checkGroups);
}



//guided sp validations=============================

//basic info
function validateStepTwo() {
  var formArray = [$('#purpose'), $('#audience'), $('#responsible_department'), $('#owning_department')];
  var errorsArray = [];
  var arrayLength = formArray.length;

  for (var i = 0; i < arrayLength; i++) {
    if (formArray[i].val().length === 0) {
      errorsArray.push(formArray[i]);
    }
  }

  if (errorsArray.length > 0) {
    renderSpErrors($("#sp-step-2"));
    return false;
  } else {
    $(".notice-error").remove();
    return true;
  }

}

//entity id
function validateStepFour() {
  var entityId = $('#entity_id');
  if (entityId.val() !== "") {
    $(".notice-error").remove();
    entityId.css('background-color', 'white');
    return true;
  } else {
    renderSpErrors($("#sp-step-4"));
    return false;
  }
}


//certificate
function validateStepFive() {
  validateCert(myCallback);
  return true;
}

//acs
//certificate
function validateStepSix() {
  if (checkAcs()) {
    $(".notice-error").remove();
    return true;
  } else {
    renderSpErrors($("#sp-step-6"));
    return false;
  }
}


//users and contacts
function validateStepThree() {
  if (checkUsers() && checkContactsNetid() && checkContactsEmail()) {
    $(".notice-error").remove();
    return true;
  }
  renderSpErrors($("#sp-step-3"));
  return false;
}


//sp in progress errors
function renderSpErrors(currentDiv) {
  var errorMsg = "";
  if (currentDiv.attr('id') === "sp-step-2") {
    errorMsg = "All fields must be filled out, or you may opt to skip this section for now."
  } else if ((currentDiv.attr('id') === "sp-step-4")) {
    errorMsg = "Please provide a valid entityID, or you may opt to skip this section for now."
  } else if ((currentDiv.attr('id') === "sp-step-5")) {
    errorMsg = "Please provide a valid certificate, or you may opt to skip this section for now."
  } else if ((currentDiv.attr('id') === "sp-step-6")) {
    errorMsg = "Please ensure you have provided an ACS for each available field, or you may opt to skip this section for now."
  } else if ((currentDiv.attr('id') === "sp-step-3")) {
    errorMsg = "Please ensure all contacts have a valid email and netid and and that all users have a valid netid, or you may opt to skip this section for now."
  }

  $('.notice-error').remove();
  currentDiv.prepend("<div class='notice-error'><p>" + errorMsg + "</p></div>");
}

function validateCurrentStep(currentStep) {
  if (currentStep === "sp-step-2") {
    return validateStepTwo();
  } else if (currentStep === "sp-step-3") {
    return validateStepThree();
  } else if (currentStep === "sp-step-4") {
    return validateStepFour();
  } else if (currentStep === "sp-step-5") {
    return validateStepFive();
  } else if (currentStep === "sp-step-6") {
    return validateStepSix();
  } else {
    return true;
  }
}
