// this logic hides form items on the SP help request page
// it only shows items that are relevant to the choices selected
function reg_logic(){
  $(".web-based").on("click",function(){
    if ($(this).find(".web-based-yes").is(":checked")) {
      $(this).nextAll(".is-vendor").hide();
      $(this).nextAll(".component-desc").hide();
      $(this).nextAll(".url").show();
      $(this).nextAll(".is-vendor").show();
      $(this).nextAll(".component-fields").show();


    }else if ($(this).find(".web-based-no").is(":checked")) {
      $(this).nextAll(".is-vendor").hide();
      $(this).nextAll("#describe-app").val("");
      $(this).nextAll(".component-desc").show();
      $(this).nextAll(".saml-check").hide();
      $(this).nextAll(".saml-check input[type='radio']").prop('checked', false);
      $(this).nextAll(".url").hide();
      $(this).nextAll(".url input[type='text']").val('');
      $(this).nextAll(".component-fields").hide();
      $(this).nextAll(".component-fields input[type='text']").val('');
    }
  });
  $(".is-vendor").on("click", function(){
    if ($(this).find(".vendor-yes").is(":checked")){
      $(this).nextAll(".saml-check").show();
      // $(this).nextAll(".component-fields").hide();

    }else if ($(this).find(".vendor-no").is(":checked")){
      $(this).nextAll(".saml-check").hide();
      $(this).nextAll(".component-fields").show();


    }
  });
}




//disable registration submit until required forms filled out
$(document).ready(function(){
  var disableButton = $('.disable-button');
  disableButton.attr('disabled', 'disabled');
  disableButton.attr('value', 'PLEASE COMPLETE FORM');
  disableButton.addClass('passive');


  $('input').on("change",function() {
    if ($(".eula").is(":checked") && radioCheck() && fieldCheck()) {
      disableButton.removeAttr('disabled');
      disableButton.attr('value', 'SUBMIT REQUEST');
      disableButton.addClass('active');
      disableButton.removeClass('passive');


    } else {
      disableButton.attr('disabled', 'disabled');
      disableButton.attr('value', 'PLEASE COMPLETE FORM');
      disableButton.removeClass('active');
      disableButton.addClass('passive');


    }
  });
});

//check required fields are filled out
function fieldCheck(){
  var filled = true;
  $('.required-field').each(function() {
    if ($(this).val() == "") {
      filled = false;
    }
  });
  return filled;
}

//check all radio and check box fields filled out
function radioCheck(){
  var saml = true;
  var vendor = true;
  var webBasedYes = $(".web-based-yes");

  if($(".vendor-yes").is(':checked')){saml = samlCheck()}
  if(webBasedYes.is(':checked')){vendor = vendorCheck()}
  if(
    (webBasedYes.is(":checked") || $(".web-based-no").is(":checked")) &&
    saml && vendor &&
    $(".eula").is(":checked")){
    return true;
  }
}


function vendorCheck(){
  if(
    ($(".vendor-yes").is(":checked") ||
    $(".vendor-no").is(":checked"))
  ){return true;}
}


function samlCheck(){
  if(
    ($(".saml-yes").is(":checked") ||
     $(".saml-no").is(":checked") ||
     $(".saml-unknown").is(":checked"))
  ){return true;}
}




