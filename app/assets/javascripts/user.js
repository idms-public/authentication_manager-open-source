//brings up a confirmation window to confirm making a work note viewable to the user
$(document).ready(function(){
  $('#confirm-public').on("click",function(){
    if($(this).prop("checked") == true) {
      var confirmation = confirm("Are you sure you want to make this note public?");
      if (confirmation) {
        $(this).prop("checked", true);
      }else{
        $(this).prop("checked", false);
      }
    }
  });
});
