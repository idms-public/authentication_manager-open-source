class ApplicationRecord < ActiveRecord::Base
	self.abstract_class = true

	def mattermost_send(message)
		conn = Faraday.new(:url => Rails.application.secrets.mattermost_webhook_url)

		conn.post do |request|
			request.headers['Content-Type'] = 'application/x-www-form-urlencoded'
			request.body = 'payload={"text":'+ message.to_json + '}'
		end
	end


	def self.grouper_group_members(group)
			members = []
			if Rails.application.secrets.use_auth_secrets
				pass = AuthSecrets::VALUES.dig('idmanager_pass')
				user = AuthSecrets::VALUES.dig('idmanager_user_id')
			else
				pass = Rails.application.secrets.grouper_user
				user = Rails.application.secrets.grouper_pass
			end

			conn = Faraday.new(:url => Rails.application.secrets.grouper_url) do |faraday|
				faraday.request  :url_encoded
				faraday.request  :basic_auth, user, pass
				faraday.response :logger                  # log requests to STDOUT
				faraday.adapter  Faraday.default_adapter  # make requests with Net::HTTP
			end
			#TODO make note in docs that members gets tagged on to the end
			response = conn.get "#{Rails.application.secrets.grouper_membership_path}#{group}/members"
			response = JSON.parse(response.body)
			response['WsGetMembersLiteResult']['wsSubjects'].each {|m| members << m['id']}

			members
	end


	def self.mock_ids(id)
		ids = JSON.parse(File.read("config/settings/mock_ids.json"))
		ids[id]
	end


	def self.ldap_search(identifier: 'duDukeID', value: nil, attrs: nil)
		settings = Rails.application.secrets

		#CONNECTION
		connection = Net::LDAP.new :host => settings.ldap_host,
												 :port => settings.ldap_port,
												 :encryption => settings.ldap_encryption.to_sym,
												 :base => settings.ldap_base,
												 :connect_timeout => settings.ldap_timeout,
												 :encoding => settings.ldap_encoding
		connection.bind ? connection : false


		#SEARCH LDAP
		filter = Net::LDAP::Filter.eq(identifier,value)
		#attrs = ["uid"] #put in what attrs you would like returned, leave nil to get all attrs
		attrs = attrs
		entries = []

		#leave out attributes or set to nil to just return all
		connection.search(:base => 'dc=duke,dc=edu' , :filter => filter,:attributes => attrs) do |entry|
			entries << entry
		end
		entries = entries[0].as_json
		if entries["myhash"]; entries["myhash"].delete("jpegphoto") end
		if entries["myhash"]; entries["myhash"].delete("duchallengeresponse")end

		entries["myhash"].to_json
	end

end
