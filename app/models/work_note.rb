class WorkNote < ApplicationRecord
	belongs_to :project, foreign_key: :project_id
	validates :note, presence: true
	after_save do
		project.update_attribute(:updated_at, Time.now)
	end

	belongs_to :component, foreign_key: :component_id

	def self.set_message(project: ,attrs: )
		original_status = project.status
		# component.update_attributes(attrs)
		"#{project.name} status changed from #{original_status} to #{attrs[:status]}"
	end

end
