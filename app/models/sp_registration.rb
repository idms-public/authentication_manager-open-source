class SpRegistration

	attr_accessor :is_admin, :notify_attr_approval
	attr_reader :sp, :entity_id, :spreg_details, :certificate, :acs, :attributes, :groups, :is_admin

	def initialize(id, is_admin: false)
		@logger = Logger.new("log/sp_reg.log")
		@sp = find_sp(id: id)
		@is_admin = is_admin

		if @sp
			@entity_id = @sp.attribute("entityID").to_s
			@spreg_details = get_details
			@certificate = get_certificate
			@acs = get_acs
			attrs = get_attributes
			@attributes = attrs[:attrs]
			@groups = attrs[:groups]
		end
	end


	#provide a list of current sps that can be used by typeahead
	def self.sps(typeahead: true)
		sps = []
		doc = File.open("vendor/shibboleth/metadata/local-sites.xml") { |f| Nokogiri::XML(f) }
		sp = doc.search("EntitiesDescriptor//EntityDescriptor")
		sp.each do |i|

			#for the typeahead remove the http https prefix for better results
			if typeahead
				i = i.attr("entityID").to_s.gsub("https://","").gsub("http://","")
				sps << i
			else
				sps << i.attr("entityID").to_s
			end
		end
		sps.to_json
	end


	#used for jquery form validation, check that spreg will accept the certificate
	def self.validate_certificate(cert)
		begin
			pem = "-----BEGIN CERTIFICATE-----\n#{cert}\n-----END CERTIFICATE-----\n"
			cert_data = OpenSSL::X509::Certificate.new(pem)
			{cert: cert,subject: cert_data.subject.to_s, issuer: cert_data.issuer.to_s, serial: cert_data.serial.to_s,
			 before: cert_data.not_before.to_s, after: cert_data.not_after.to_s}
		rescue
			false
		end
	end

	def self.mattermost_send(message)
		conn = Faraday.new(:url => 'https://mattermost.oit.duke.edu/hooks/ompskygbkid89bxzrck7xt48ca')

		conn.post do |request|
			request.headers['Content-Type'] = 'application/x-www-form-urlencoded'
			request.body = 'payload={"text":'+ message.to_json + '}'
		end
	end

	#find the sp entry in the local-sites.xml file, use recursion to find correct prefix, e.g. https,http,nil
	def find_sp(protocol: "https://", doc: nil, id: nil)
		unless doc
			doc = File.open("vendor/shibboleth/metadata/local-sites.xml") { |f| Nokogiri::XML(f) }
		end
		sp = doc.search("EntitiesDescriptor//EntityDescriptor[entityID='#{protocol}#{id}']")
		if sp.to_a != []
			return sp
		elsif sp.to_a == [] && protocol == "https://"
			find_sp(protocol: "http://", doc: doc ,id: id)
		elsif sp.to_a == [] && protocol == "http://"
			find_sp(protocol: "", doc: doc ,id: id)
		else
			false
		end
	end


	#grab certificate from registration
	def get_certificate
		full_cert = @sp[0].children.search("SPSSODescriptor//KeyDescriptor").text.strip
		pem = "-----BEGIN CERTIFICATE-----\n#{full_cert}\n-----END CERTIFICATE-----\n"
		cert = OpenSSL::X509::Certificate.new(pem)

		{full_cert: full_cert,subject: cert.subject.to_s, issuer: cert.issuer.to_s, serial: cert.serial.to_s,
		 before: cert.not_before.to_s, after: cert.not_after.to_s}
	end


	#grab acs from registration
	def get_acs
		acs = []

		@sp[0].children.search("AssertionConsumerService").each do |a|
			default = a.attr("isDefault").to_s
			default == 'true' ? default = true : default = false
			acs << {location: a.attr("Location").to_s, binding: a.attr("Binding").to_s, is_default: default }
		end
		acs
	end

	#
	#get all isMemberOf groups from a registration
	def get_groups(isMemberOf)
		groups = []
		if isMemberOf.children.children.empty?
			isMemberOf.children.each {|x| groups << {group: x.attribute("value").to_s, regex: false}}
			isMemberOf.children.each {|x| groups << { group: x.attribute("regex").to_s, regex: true}}
		else
			isMemberOf.children.children.each {|x| groups << {group: x.attribute("value").to_s, regex: false}}
			isMemberOf.children.children.each {|x| groups << {group: x.attribute("regex").to_s, regex: true}}
		end
		groups.delete_if {|g|  g[:group].empty?} #get rid of any empty values
	end


	#grab all attributes associated with the registration
	def get_attributes
		attributes = []
		groups = []
		doc = File.open("vendor/shibboleth/conf/attribute-filter.xml") { |f| Nokogiri::XML(f) }

		attrs = doc.search("AttributeFilterPolicyGroup//AttributeFilterPolicy[id='#{@entity_id}']")

		#iterate though attribute filter and grab all released attrs and groups if isMemberOf is present
		attrs.search("AttributeRule").each do |i|
			if i.attr("attributeID").to_s == "isMemberOf"
				groups = get_groups(i)
			else
				attr = i.attr("attributeID").to_s
				is_public = Spreg::Attribute.find_by(attributeID: attr).is_public
				attributes << {name: attr, is_public: is_public}
			end
		end

		{attrs: attributes, groups: groups}
	end


	#gather details stored in the spreg database
	def get_details
		contacts = []
		users = []
		sp = Spreg::Sp.find_by(entity_id: @entity_id)
		if sp.nil?; raise SpRegistration::MissingSpError end
		details = {name: sp.public_name, purpose: sp.functional_purpose, audience: sp.audience,
							 responsible_dept: sp.responsible_dept, owner_dept: sp.function_owner_dept,sp_id: sp.id}
		sp_contacts = sp.sp_contacts
		sp_contacts.each {|c| contacts << {type: c.con_type, netid: c.netid, email: c.email}}


		sp_users = sp.users
		sp_users.each {|u| users << u.netid}

		details[:contacts] = contacts
		details[:users] = users

		details
	end





	#####
	# BEGIN SPREG LOGIC
	#####
	def save(update_values, user_id)
		entity_id = nil
		if update_values.has_key?(:base_entity_id)
			entity_id = update_values[:base_entity_id].strip  # only on an update?
		else
			entity_id = update_values[:entity_id].strip
		end

		results = {:success => true, :messages => []}

		sp = Spreg::Sp.find_or_create_by(entity_id: entity_id)

		#if SpRegistration.isEntityIdInInCommon(entity_id)
		#	results[:messages] << "Entity registered through InCommon"
		#	results[:success] = false
		#	return results
		#end

		# don't continue if there's a pending request
		# todo error handling - error messages here
		if sp.pending_sps.size > 0
			results[:messages] << "There are pending changes for this entity id"
			results[:success] = false
			return results
		end

		if SpRegistration.detail_updates?(update_values)

			unless save_sp(sp, update_values)
				results[:success] = false
				results[:messages] << "Failed to save details"
				return results
			end

		end


		if update_values.has_key?(:users)
			current_users = []
			current_users = @spreg_details[:users] if !@sp.nil? && !@spreg_details.nil? && @spreg_details.has_key?(:users)

			unless save_user_updates(sp, current_users, update_values[:users])
				results[:success] = false
				results[:messages] << "Failed to save users"
			end
		end

		if (update_values.has_key?(:contacts))
			current = []
			current = @spreg_details[:contacts] if !@sp.nil? && !@spreg_details.nil? && @spreg_details.has_key?(:contacts)

			unless save_contacts(sp, current, update_values[:contacts])
				results = false
				results[:messages] << "Failed to save contacts"
			end

		end

		# populates pending tables
		if SpRegistration.reg_changes?(update_values)

			unless save_pending_updates(sp,update_values, user_id)
				results[:success] = false
				results[:messages] << "Failed to save pending changes"
				rollback_pending_updates(sp)

				results[:messages] << "Rolled back pending changes"
			end

			@logger.info("save return updates #{results.inspect}")
			return results
		end

		@logger.info("save return no updates #{results.inspect}")
		return results

	end

	def save_pending_updates(sp, update_values, user_id)

		if save_pending_acs(sp, update_values) &&
			save_pending_groups(sp, update_values) &&
			save_pending_attrs(sp, update_values) &&
			save_pending_sp(sp, update_values, user_id)

			@logger.info("checking for pending attributes that need to be approved")
			pending = sp.pending_sp_attrs.find_by(is_approved: 0)
			# if Rails.env != 'test'
			if (!pending.nil?)
				entity = sp.entity_id
				message = """Pending Attribute approval at https://idms-web-authn-test-01.oit.duke.edu/manager/admin/spreg
        ## Details
        #{entity}
				"""

				SpRegistration.mattermost_send message if Rails.env != 'test'

			end

			@logger.info("save_pending_updates return success")
			return true

		else
			@logger.info("save_pending_updates return fail")
			return false
		end


	end

	def save_pending_groups(sp, update_values)
		groups = []

		begin
			if update_values.has_key?(:groups)

				update_values[:groups].each do |group|

					# this shouldn't be needed. make sure the new and existing sps are cleaned up - there's method for that
					# removes empty groups, paths, etc
					# next  group.has_key?(:path) && group[:path].blank?

					puts "path=#{group[:path]} is_value_regex=#{group[:is_value_regex]}"

					regex = group.has_key?(:is_value_regex) && group[:is_value_regex] == 'true' ? true  : false
					reason = group.has_key?(:reason) ? group[:reason] : nil

					approved = false
					if @is_admin
						approved = true
					elsif SpRegistration.is_group_already_approved(group[:path], @groups)
						approved = true
					end

					groups << {group: group[:path], regex: regex, reason: reason, approved: approved }

				end

			elsif @groups
				# make sure reason is in there
				groups = @groups
				groups.each do |group|
					group[:approved] = true
				end
			end

			# this gets the attribute id and permissions for isMemberOf
			attribute = Spreg::Attribute.find_by(attributeID: 'isMemberOf')

			groups.each do |group|

				reason = group[:reason]

				Spreg::PendingSpAttr.create(sp_id: sp.id,
																		attribute_id: attribute.id,
																		value: group[:group],
																		is_value_regex: group[:regex],
																		is_approved: group[:approved],
																		reason: reason)

			end

		rescue => e
			@logger.error("Failed updating PendingSpAttribute groups for #{sp.id} message=#{e.message}" )
			return false

		end

		return true
	end


	def save_pending_attrs(sp, update_values)
		attrs = []

		begin

			if update_values.has_key?(:attributes)

				update_values[:attributes].each do |attr|
					@logger.info("10 #{attr}")
					attribute = Spreg::Attribute.find_by(attributeID: attr[:attr])
					reason = attr.has_key?(:reason) ? attr[:reason] : nil
					approved = false
					if @is_admin
						approved = true
					elsif attribute.is_public == true
						approved = true
					elsif SpRegistration.is_attr_already_approved(attr[:attr], @attributes)
						approved = true
					end

					#:notify_attr_approval = true if !approved
					attrs << {name: attr[:attr], reason: reason, approved: approved, id: attribute.id}
				end

			elsif @attributes
				attrs = @attributes
				attrs.each do |attr|
					attribute = Spreg::Attribute.find_by(attributeID: attr[:name])
					attr[:approved] = true
					attr[:id] = attribute.id
				end

			end

			attrs.each do |attr|
				reason = attr.has_key?(:reason) ? attr[:reason] : nil

				Spreg::PendingSpAttr.create(sp_id: sp.id,
																		attribute_id: attr[:id],
																		is_value_regex: 0,
																		is_approved: attr[:approved],
																		reason: reason)
			end
		rescue => e
			@logger.error("Failed updating PendingSpAttribute attributes for #{sp.id} message=#{e.message}")
			return false
		end

		return true

	end


	def save_pending_acs(sp, update_values)
		acss = []

		begin
			if (update_values.has_key?(:acs))
				acss = update_values[:acs]
			elsif !@acs.nil?
				acss = @acs
			else
				@logger.error("ACS is required sp=#{sp.id}")
			end

			if acss
				acss.each do |acs|
					location = acs[:location]
					binding = acs[:binding]
					default = acs.has_key?(:is_default) && acs[:is_default] == "on" ? 1 : 0
					delete = 0 # don't think this is used

					Spreg::PendingSpAssertion.create(sp_id: sp.id,
																					 url: location,
																					 binding: binding,
																					 is_default: default,
																					 is_delete: delete)

				end
			end

		rescue => e
			@logger.error("Failed updating PendingSpAssertion for #{sp.id} mesage=#{e}")
			return false

		end

		return true
	end


	def save_pending_sp(sp, update_values, user_id)
		new_entity_id = nil

		begin
			if update_values.has_key?(:entity_id)
				new_entity_id = update_values[:entity_id]
			else
				new_entity_id = update_values[:base_entity_id] # todo check this id
			end

			is_api_request = 0
			if update_values.has_key?("is_api_request")
				is_api_request = update_values[:is_api_request]
			end

			certificate = nil
			if update_values.has_key?(:cert)
				certificate = update_values[:cert]
			elsif !@certificate.nil? && @certificate.has_key?(:full_cert)
				certificate = @certificate[:full_cert]
			end

			attributes = {sp_id: sp.id, user_id: user_id, new_entity_id: new_entity_id, certificate: certificate, is_api_request: is_api_request}

			Spreg::PendingSp.create(attributes)

		rescue => e
			@logger.error("Failed updating PendingSp for sp=#{sp.id} message=#{e}")
			return false
		end

		return true

	end


	def save_contacts(sp, current, updates)

		begin
			deletes = current - updates
			additions = updates - current

			deletes.each do |contact|
				spc = Spreg::SpContact.find_by(sp_id: sp.id, netid: contact[:netid], con_type: contact[:type])
				spc.destroy
			end

			additions.each do |contact|
				if Spreg::SpContact.find_or_create_by(sp_id: sp.id, netid: contact[:netid], con_type: contact[:type], email: contact[:email])
					# nothing
				else
					return false
				end
			end

		rescue => e
			@logger.error"Failed updating SpContact for sp=#{sp.id} message="#{e})
			return false
		end

		return true

	end
	#
	# def save_owner_updates(sp, current, updates)
	# 	begin
	# 		deletes = current - updates
	# 		additions = updates - current
	#
	# 		deletes.each do |user|
	# 			o = Spreg::Owner.find_by(owner: user[:owner], owner_type: user[:owner_type])
	# 			sp.owners.destroy(o)
	# 		end
	#
	# 		additions.each do |user|
	# 			o = Spreg::Owner.find_or_create_by(owner: user[:owner], owner_type: user[:owner_type])
	# 			sp.owners << o
	# 		end
	# 	rescue
	# 		return false
	# 	end
	#
	# 	return true
	# end

	def save_user_updates(sp, current, updates)

		begin
			deletes = current - updates
			additions = updates - current

			deletes.each do |user|
				u = Spreg::User.find_by(netid: user)
				sp.users.destroy(u)
			end

			additions.each do |user|
				u = Spreg::User.find_or_create_by(netid: user)
				sp.users << u
			end

		rescue
			return false
		end

		return true
	end

	def save_sp(sp, update_values)

		begin
			attributes = {}
			attributes[:functional_purpose] = update_values[:purpose]
			attributes[:responsible_dept] = update_values[:responsible_department]
			attributes[:function_owner_dept] = update_values[:owning_department]
			attributes[:audience] = update_values[:audience]
			if update_values.has_key?(:is_api_request)
				attributes[:is_api_request] = update_values[:is_api_request]
			end

			if sp.update(attributes)
				return true
			else
				# get error messages and set
				return false
			end
		rescue
			return false
		end

		return true

	end

	def delete(user_id)
		begin
			user = Spreg::User.find_by(netid: user_id) # can also look into sp.users for user id to make sure is OK
			sp = Spreg::Sp.find_by(entity_id: @entity_id)
			sp.pending_sps << Spreg::PendingSp.new(new_entity_id: @entity_id, is_delete: true, user_id: user.id, sp_id: sp.id)
		rescue => e
			@logger.error e
			return false
		end
		return true
	end

	# private
	private unless 'test' == Rails.env

	def self.detail_updates?(my_params)

		check_params = [:purpose, :responsible_department, :owning_department, :audience]
		check_params.each do |key|
			return true if my_params.key?(key)
		end

		return false
	end

	def self.reg_changes?(my_params)
		check_params = [:cert, :newAttr, :acs, :is_member_of, :contacts]
		# also check if base_entity_id == entity_id
		# check if attrs is empty
		# contacts too
		check_params.each do |key|
			return true if my_params.key?(key)
		end

		return true if (my_params[:base_entity_id] != my_params[:entity_id])

		# check on this behavior from ui
		# return true if (my_params.key?(:attrs) && !my_params[:attrs].empty?)

		return false

	end

	def rollback_pending_updates(sp)

		# if new should remove even sp but not existing sps
		# how to identify new vs. existing
		sp.pending_sps.each {|p| p.destroy}
		sp.pending_sp_assertions.each {|p| p.destroy}
		sp.pending_sp_attrs.each {|p| p.destroy}
		@logger.info("Rollback of pending changes sp=#{sp.id}")
	end

	def self.is_group_already_approved(group, groups)
		return false if groups.nil?

		groups.each {|grp| return true if grp[:group] == group}
		return false
	end

	def self.is_attr_already_approved(attr, attrs)
		return false if attrs.nil?

		attrs.each {|a| return true if a[:name] == attr}
		return false
	end

	def self.isEntityIdInInCommon(entityID)
		doc = File.open("vendor/shibboleth/metadata/InCommon-metadata.xml") { |f| Nokogiri::XML(f) }
		sp = doc.search("EntitiesDescriptor//EntityDescriptor[entityID='#{entityID}']")
		return sp.to_a != []
	end


	def self.isEntityIdInLocalSites(entityID)
		doc = File.open("vendor/shibboleth/metadata/local-sites.xml") { |f| Nokogiri::XML(f) }
		sp = doc.search("EntitiesDescriptor//EntityDescriptor[entityID='#{entityID}']")
		return sp.to_a != []
	end
end

class SpRegistration::MissingSpError < StandardError
end
