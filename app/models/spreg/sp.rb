class Spreg::Sp < Spreg::Base
	has_many :sp_contacts
  has_many :pending_sps, :class_name => 'Spreg::PendingSp'
  has_many :pending_sp_assertions, :class_name => 'Spreg::PendingSpAssertion'
  has_many :pending_sp_attrs, :class_name => 'Spreg::PendingSpAttr'
	has_and_belongs_to_many :users
  has_and_belongs_to_many :owners, :class_name => 'Spreg::Owner'
	#accepts_nested_attributes_for :sp_contacts
end