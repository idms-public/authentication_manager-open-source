class Spreg::Base < ApplicationRecord
	if Rails.application.secrets.legacy_db
		if Rails.env == "production"
			establish_connection :sql_production
		else
			establish_connection :sql_development
		end
	end

	self.abstract_class = true

	def self.gendata
		xml_string = <<-XML
<?xml version="1.0" encoding="UTF-8" standalone="no"?>
<AttributeFilterPolicyGroup xmlns="urn:mace:shibboleth:2.0:afp" xmlns:basic="urn:mace:shibboleth:2.0:afp:mf:basic" xmlns:saml="urn:mace:shibboleth:2.0:afp:mf:saml" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" id="ShibbolethFilterPolicy" xsi:schemaLocation="urn:mace:shibboleth:2.0:afp classpath:/schema/shibboleth-2.0-afp.xsd                         urn:mace:shibboleth:2.0:afp:mf:basic classpath:/schema/shibboleth-2.0-afp-mf-basic.xsd                         urn:mace:shibboleth:2.0:afp:mf:saml classpath:/schema/shibboleth-2.0-afp-mf-saml.xsd">

</AttributeFilterPolicyGroup>
		XML
		File.write('vendor/shibboleth/conf/attribute-filter.xml', xml_string)
	end

	def self.adddata
		doc = File.open("vendor/shibboleth/conf/attribute-filter.xml") { |f| Nokogiri::XML(f) }
		ed = doc.search("AttributeFilterPolicyGroup").first
		new_xml = <<-XML
	<AttributeFilterPolicy id="https://idms-web-netid-test-01.oit.duke.edu/shibboleth">
    <PolicyRequirementRule value="https://idms-web-netid-test-01.oit.duke.edu/shibboleth" xsi:type="basic:AttributeRequesterString"/>
    <AttributeRule attributeID="uid">
      <PermitValueRule xsi:type="basic:ANY"/>
    </AttributeRule>
	  <AttributeRule attributeID="mail">
      <PermitValueRule xsi:type="basic:ANY"/>
    </AttributeRule>
	  <AttributeRule attributeID="sn">
      <PermitValueRule xsi:type="basic:ANY"/>
    </AttributeRule>
	  <AttributeRule attributeID="eduPersonPrimaryAffiliation">
      <PermitValueRule xsi:type="basic:ANY"/>
    </AttributeRule>
	  <AttributeRule attributeID="isMemberOf">
      <PermitValueRule xsi:type="basic:OR">
      <basic:Rule value="urn:mace:duke.edu:groups:oit:csi:is:idmtech" xsi:type="basic:AttributeValueString"/>
      <basic:Rule regex="urn:mace:duke.edu:groups:oit:idm:applications:affiliates:.*" xsi:type="basic:AttributeValueRegex"/>
      <basic:Rule regex="urn:mace:duke.edu:groups:oit:idm:applications:onelink:.*" xsi:type="basic:AttributeValueRegex"/>
      <basic:Rule value="urn:mace:duke.edu:groups:dhts:dhead:dukemedicalmembers" xsi:type="basic:AttributeValueString"/>
      <basic:Rule regex="urn:mace:duke.edu:groups:oit:idm:applications:identity-manager:.*" xsi:type="basic:AttributeValueRegex"/>
      </PermitValueRule>
    </AttributeRule>
		<AttributeRule attributeID="givenName">
	    <PermitValueRule xsi:type="basic:ANY"/>
	  </AttributeRule>
	</AttributeFilterPolicy>
		XML
		ed.add_child(new_xml)
		File.write('vendor/shibboleth/conf/attribute-filter.xml', doc.to_xml)
	end

end
