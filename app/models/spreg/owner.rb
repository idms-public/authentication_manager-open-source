class Spreg::Owner < Spreg::Base

  has_and_belongs_to_many :sps, :class_name => 'Spreg::Sp'

  def get_entity_ids
    begin
      ids = []
      sps = self.sps
      sps.each do |sp|
        reg = {}
        reg[:entity_id] = sp.entity_id
        reg[:name] = sp.public_name
        reg[:last_update] = sp.updated_at
      end
    rescue
      false
    end
  end
end
