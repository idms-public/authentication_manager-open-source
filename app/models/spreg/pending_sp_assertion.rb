class Spreg::PendingSpAssertion < Spreg::Base
  belongs_to :sp
  belongs_to :spreg_sp, :class_name => 'Spreg::Sp', foreign_key: :sp_id
end
