class Spreg::User < Spreg::Base
	has_and_belongs_to_many :sps


	def get_entity_ids
		begin
			ids = []
			sps = self.sps
			sps.each do |sp|
				reg = {}

				reg[:entity_id] = sp.entity_id
				reg[:name] = sp.public_name
				reg[:last_update] = sp.updated_at


				ids << reg
			end
			ids
		rescue
			false
		end
	end


end