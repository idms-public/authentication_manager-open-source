class Project < ApplicationRecord
	validates_acceptance_of :terms_of_service
	has_many :work_notes
	has_many :contacts
	accepts_nested_attributes_for :contacts

	scope :working, -> {where('status=? OR status=?', 'Integration Requested','In Progress').order(updated_at: :desc)}
	scope :closed, -> {where('status=? OR status=?', 'Closed/Complete','Closed/Incomplete').order(updated_at: :desc)}
	scope :global_search, -> (search) {where("lower(name) LIKE ? OR lower(sponsor) LIKE ?", "%#{search}%","%#{search}%")}
	scope :university, -> {where(institution: "University").order(updated_at: :desc)}
	scope :health, -> {where(institution: "Health System").order(updated_at: :desc)}



	validates :name,:sponsor,:sponsor_duid,:sponsor_email,:help_details, presence: true


	def self.admins
		#user grouper and ldap to populate an admin group
		if Rails.application.secrets.grouper_admin_source
			members = "idm-members".to_sym
			Rails.cache.fetch(members, expires_in: 12.hours) do
				admins = []
				admin_groups = JSON.parse(File.read('config/settings/grouper_admin_groups.json')).dig("groups")
				admin_groups.each do |g|
					group = grouper_group_members(g)
					group.each {|i| admins << JSON.parse(ldap_search(value: i, attrs: ['displayname'])).dig("displayname",0)}
				end
				return admins.compact
			end
		else
			final_admins = []
			admins = JSON.parse(File.read('config/settings/admins.json'))["admins"].compact
			admins.compact.each {|i| final_admins << "#{i["displayname"]} (#{i["uid"]})"}
			final_admins
		end
	end



	def custom_errors
		errors.add(:name,"Project name must be filled out")
		errors.add(:terms_of_service,"You must read and accept the Partnership Agreement")
		errors.add(:help_details, "Please tell us how you would like OIT to help")
		errors.add(:is_vendor,"Must indicate if project is developed by a vendor")
		errors.add(:is_web_based ,"Must indicate if project is web based")
	end


	def mattermost_alert(alert: nil, work_note: nil)
		if Rails.application.secrets.use_mattermost

			Rails.env != "production" ? message = "TESTING\n" : message = ""
			if alert == 'project-assigned'
				message += "#{self.name} has been assigned to: #{self.analyst}."
			elsif alert == 'project-updated' || 'note-added'
				message += """#{self.name} has been updated:\n
				## Details

				STATUS: #{self.status}\n
				ANALYST: #{self.analyst}\n
				NOTES: #{work_note.note if work_note}
				"""
			else
				message += """#{self.sponsor} has asked for help with a shibboleth integration titled: #{self.name}. Please reach out to #{self.sponsor} at #{self.sponsor_email}. See details of project below:
				## Details
				#{self.help_details}
				"""
			end
			if Rails.env != "test" && alert; mattermost_send(message) end

		end
	end

end
