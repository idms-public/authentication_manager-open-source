class AdminController < ApplicationController
	layout 'admin'
	before_action :authorize_admin

	#get
	def index
		@projects = filter_project(session[:active_filter]).working
		render "index", locals: {closed: false}
	end

	#get
	def closed
		@projects = filter_project(session[:active_filter]).closed
		render "index", locals: {closed: true, header: "Closed Integrations"}

	end

	#get
	def on_hold
		@projects = filter_project(session[:active_filter]).where(status: "On Hold").order(updated_at: :desc)
		render "index", locals: {closed: true,header: "Integrations On Hold"}
	end


	#post
	def search_projects
		search = params[:search].downcase
		@projects = Project.global_search(search)
		render "index", locals: {closed: true, header: "Archived"}
	end


	#get
	def show
		@project = Project.find_by id: params[:id]
		@admins = Project.admins

		@work_note = WorkNote.new
		@work_notes = WorkNote.where(project_id:  params[:id]).order(created_at: :desc)
	end


	#post
	def assign_analyst
		project = Project.find_by id: params[:id]

		response = project.update(analyst: params[:analyst_assign])
		if response
			flash[:success] = "Project Assigned"
			project.mattermost_alert(alert: 'project-assigned')
		else
			flash[:error] = "Error Assigning Project"
		end
		redirect_back(fallback_location: root_path)
	end


	#post
	def update_status
		#todo refactor into a model
		safe_params = project_params(params)
		project = Project.find_by(id:params[:id])
		update_message = WorkNote.set_message(project: project,attrs: safe_params)
		work_note = WorkNote.new(project_id: project.id, note: update_message, updated_by: @shib_attrs.dig(:display_name),is_public: true)
		note_saved = work_note.save


	 	result = project.update(status: safe_params[:status])
		if result && note_saved
			flash[:success] = "Project Updated"
			project.mattermost_alert(alert: 'project-updated', work_note: work_note)
		else
			flash[:error] = "Error Updating Project"
		end
		redirect_back(fallback_location: root_path)
	end

	#post
	def create_note
		project = Project.find_by_id params['work_note']['project_id']
		@work_note = WorkNote.new(work_note_params(params))
		@work_note.updated_by = @shib_attrs.dig(:display_name)
		if @work_note.save
			flash[:success] = "Work Note Added."
			project.mattermost_alert(alert: 'project-updated', work_note: @work_note)
			redirect_back(fallback_location: root_path)
		else
			flash[:error] = "Please enter a work note."
			redirect_back(fallback_location: root_path)
		end
	end


	#patch
	def update_note
		work_note = WorkNote.find_by id: params[:id]
		if work_note.update(work_note_params(params))
			flash[:success] = "Work Note Updated."
			redirect_back(fallback_location: root_path)
		else
			flash[:error] = "Error Updating Work Note"
			redirect_back(fallback_location: root_path)
		end
	end

	#patch
	def assign_institution
		project = Project.find_by_id(params[:id])
		if project.update(project_update_params(params))
			flash[:success] = "Project Updated."
			redirect_back(fallback_location: root_path)
		else
			flash[:error] = "Error Updating Project"
			redirect_back(fallback_location: root_path)
		end
	end


	#delete
	def delete_note
		work_note = WorkNote.find_by id: params[:id]
		if work_note.delete
			flash[:success] = "Work note deleted."
			redirect_back(fallback_location: root_path)
		else
			flash[:error] = "Failed to delete work note."
			redirect_back(fallback_location: root_path)
		end
	end


	#post
	def set_active_filter
		if params[:org] == "university"
			session[:active_filter] = "university"
		elsif params[:org] == "health"
			session[:active_filter] = "health"
		else
			session[:active_filter] = nil
		end
		redirect_back(fallback_location: root_path)
	end


	private
	def work_note_params(my_params)
		my_params.require(:work_note).permit(:note,:project_id,:is_public)
	end

	def project_update_params(my_params)
		my_params.require(:project).permit(:institution)
	end

	def project_params(my_params)
		my_params.permit(:status,:work_note,:employee,)
	end


	def filter_project(org)
		if org == "university"
			Project.university
		elsif org == "health"
			Project.health
		else
			Project
		end
	end
end
