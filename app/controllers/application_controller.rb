class AuthenticationManager::UnauthorizedAction < StandardError
end

class AuthenticationManager::UnknownError < StandardError
end

class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
	if Rails.application.secrets.auth_type == "samlCamel"
    before_action :saml_protect
	end
  if (Rails.env == "development" || Rails.env == "test")
    before_action :mock_shib, except: [:logout, :stop_mock]
  end

	before_action :set_layout_variables, except: [:logout, :stop_mock]

	rescue_from AuthenticationManager::UnauthorizedAction, :with => :auth_rescue
	rescue_from SpRegistration::MissingSpError, :with => :missing_sp
  rescue_from AuthenticationManager::UnknownError, :with => :unknown_rescue

	###
	# Shibboleth
	###
	def mock_shib
		if session[:mock_id]
			identity = session[:mock_id]
			identity = SamlCamel::Shib.create_identity(identity)
			SamlCamel::Shib.set_headers(request: request, identity: identity)
    #handle authentication if developer wants to only mock shib headers
    # elsif Rails.application.secrets.auth_type == "none" && session[:mock_id] != false
    #   session[:mock_id] =  {"AJP_displayName"=>"noShibAdmin", "AJP_eppn"=>"noShibAdmin", "AJP_duDukeID"=>"noShibAdmin", "AJP_isMemberOf"=>"urn:mace:duke:oit:idm:applications:authentication-manager:admins"}
    #   identity = SamlCamel::Shib.create_identity(session[:mock_id])
		# 	SamlCamel::Shib.set_headers(request: request, identity: identity)
		# else
			# SamlCamel::Shib.set_headers(request: request)
		end
	end


  def stop_mock
    session[:mock_id] = nil
    redirect_back(fallback_location: root_path)
  end


	def set_layout_variables
    if session[:mock_id] || Rails.application.secrets.auth_type == "shib"
	    @shib_attrs = SamlCamel::Shib.attributes(request)
      @shib_attrs["isMemberOf"] = @shib_attrs["isMemberOf"].try(:split, ";") || "no groups present"
      @shib_attrs = @shib_attrs.symbolize_keys
    elsif Rails.application.secrets.auth_type == "samlCamel"
      samlCamel = session[:saml_attributes]
      @shib_attrs = {eppn: samlCamel.dig("eduPersonPrincipalName", 0), du_proxy_token: nil, du_duke_id: samlCamel.dig("duDukeID",0), display_name: samlCamel.dig("displayName", 0),
                    isMemberOf: samlCamel["isMemberOf"], mail: samlCamel.dig("mail", 0), uid: samlCamel.dig("uid", 0)}
    elsif Rails.application.secrets.auth_type == "mock"
      @shib_attrs = {eppn: "mockAdmin", du_proxy_token: "mockAdmin", du_duke_id: "mockAdmin", display_name: "mockAdmin", isMemberOf: "mockAdmin", mail: "mockAdmin", uid: "mockAdmin"}
    end
	end


  #checks isMemberOf shib attributes for grouper group membership
	def authorize_admin
    return true if @shib_attrs[:eppn] == "mockAdmin" &&  Rails.application.secrets.auth_type == "mock"
    if Rails.application.secrets.grouper_admin_source
  		unless @shib_attrs[:isMemberOf].include?(Rails.application.secrets.grouper_admin_group) ||
  			     @shib_attrs[:isMemberOf].include?("urn:mace:duke.edu:groups:oit:idm:applications:authentication-manager:test_admins")
  			raise AuthenticationManager::UnauthorizedAction
  		end
    else
      final_admins = []
      admins = JSON.parse(File.read('config/settings/admins.json'))["admins"].compact
      admins.each {|i| final_admins << i["uid"]}
      raise AuthenticationManager::UnauthorizedAction unless @shib_attrs.dig(:uid).in?(final_admins)
    end
	end


	def is_admin
    return true if @shib_attrs[:eppn] == "mockAdmin" &&  Rails.application.secrets.auth_type == "mock"

    if Rails.application.secrets.grouper_admin_source
    	if @shib_attrs[:isMemberOf].include?(Rails.application.secrets.grouper_admin_group) ||
    		@shib_attrs[:isMemberOf].include?("urn:mace:duke.edu:groups:oit:idm:applications:authentication-manager:test_admins")
    		true
    	end
    else
      final_admins = []
      admins = JSON.parse(File.read('config/settings/admins.json'))["admins"].compact
      admins.each {|i| final_admins << i["uid"]}
      return true if @shib_attrs.dig(:uid).in?(final_admins)
    end
	end


  #check if user is either an admin or an sp owner
	def authorize_sp_user(sp)
    sp_registration = Spreg::Sp.find_by(entity_id: sp.entity_id)
    raise AuthenticationManager::UnknownError unless sp_registration
    sp_users = sp_registration.users.all.pluck(:netid)
		if is_admin || sp_users.include?(@shib_attrs[:uid])
			true
		else
			raise AuthenticationManager::UnauthorizedAction
		end
	end


	#post
	def select_mock_id
		session[:mock_id] = ApplicationRecord.mock_ids(params[:mock_id])
		redirect_back(fallback_location: root_path)
	end


  def logout
    reset_session
      redirect_to Rails.application.secrets.logout_url
  end
##### end shib

	private

	def missing_sp
		flash[:notice] = "No SP Registration Found. This is most likely an SP not managed through SPREG. Search local-sites.xml file on IDP."
		redirect_to spreg_index_path
	end

	def auth_rescue
		render :file => 'public/401.html',:status => 401 and return
	end

  def unknown_rescue
    render :file => 'public/500.html',:status => 500 and return
  end
end
