class Admin::SpregController < ApplicationController
	layout 'admin'
	include SpregHelper
	before_action :authorize_admin, only: ["index","search","sp_list","attribute_approval"]
	# before_action :authorize_sp_user, only: ["show","update"]


	#get
	def index
		@pending_sp_attrs = Spreg::PendingSpAttr.where(is_approved: false)
	end


	#get
	def show
		entity_id = params[:entityId].strip
		@sp = SpRegistration.new(entity_id)
		authorize_sp_user(@sp)
		@is_admin = is_admin
		if @sp.sp
			@attributes = Spreg::Attribute.where(available: 1).pluck(:attributeID,:is_public) #attributes to populate drop down
			@pending = Spreg::PendingSp.find_by new_entity_id: @sp.entity_id
		else
			redirect_to action: 'index'
		end
	end


	def search_by_user
		user = Spreg::User.find_by(netid: params[:netid])
		if user
			@pending_sp_attrs = Spreg::PendingSpAttr.where(is_approved: false)
			@potential_matches = user.sps.pluck(:entity_id)
			render "index"
		else
			@pending_sp_attrs = Spreg::PendingSpAttr.where(is_approved: false)
			flash[:error] = "Unable to any SPs registered to #{params[:netid]}"
			redirect_to action: 'index'
		end
	end

	#post
	def search
		potential_matches = []
		sps = JSON.parse(SpRegistration.sps(typeahead: false))
		sps.each{ |sp| potential_matches << sp if sp.match(params[:entityId]) }

		if potential_matches.size > 1
			@pending_sp_attrs = Spreg::PendingSpAttr.where(is_approved: false)
			@potential_matches = potential_matches
			render "index"
		elsif potential_matches.size == 1
			redirect_to show_sp_path(entityId: potential_matches.join(""))
		else
			flash[:error] = "Unable to find entity ID #{params[:entityId]}"
			redirect_to action: 'index'
		end
	end


	#get - render sps in json for typeahead
	def sp_list
		sps = SpRegistration.sps
		render json: sps
	end


	#put
	#FIXME sort this and edit out with teddy
	def update
		entityId = params[:entityId].strip
		@sp = SpRegistration.search(entityId)
		authorize_sp_user(@sp)
		render json: @sp
	end


	def attribute_approval
		# should be admin
		# 1. get total_option approve/disapprove
		# 2. get ids and delete or update or this should be 1

		#TODO pass in params and move into a model
		if params.has_key?(:total_option)
			ids = []

			ids = params[:approval_action]
			if params[:total_option] == 'approve'
				ids.each do |id|
					pending_attr = Spreg::PendingSpAttr.find(id)
					pending_attr.update({is_approved: 1}) if !pending_attr.nil?
				end
			elsif params[:total_option] == 'disapprove'
				ids.each do |id|
					pending_attr = Spreg::PendingSpAttr.find(id).destroy
				end
			end
		end
		redirect_back(fallback_location: root_path)
	end
end
