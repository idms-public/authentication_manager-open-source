require 'jwt'
require 'net-ldap'
require "./config/initializers/auth_secrets.rb"

class UnauthorizedAction < StandardError
  def initialize(msg="Authentication failed")
    super(msg)
  end
end

class RequestValidationException < StandardError
  def initialize(msg)
    super(msg)
  end
end

class Admin::ApiController < ActionController::Base
  @username = nil

  before_action :authenticate_user!

  protect_from_forgery with: :null_session

  rescue_from UnauthorizedAction, :with => :auth_rescue

  @secrets = AuthSecrets::VALUES

  def create

    is_admin = false

    doc =  Nokogiri::XML(request.body.read) do |config|
      config.strict.noblanks
    end

    # xml = request.body.read
    # logger.info(xml)
    # doc =  Nokogiri::XML(xml)

    logger.info(doc)

    update_values = {}
    update_values[:is_api_request] = true  # of course
    required_values = {entity_id: 0,public_name: 0,functional_purpose: 0, function_owner_dept: 0,audience: 0,environment: 0,new_user_attributes: 0,new_pending_sp_attributes: 0,new_pending_sp_assertion_attributes: 0}

    begin

      doc.xpath("sp").children.each do |n|
        puts "#{n.name} : #{n.text}"

        name = n.name

        logger.info("node #{name}")

        if name == 'entity_id'
          entity_id = n.text
          valid_entity_id = /^https?:\/\/[\w.:\/-]+$/
          unless valid_entity_id.match(entity_id)
            raise RequestValidationException.new("Invalid entity id")
          end

          unless (uniqueEntityId(entity_id))
            raise RequestValidationException.new("Entity Id already in use")
          end

          # entity_id.reg
          update_values[:entity_id] = entity_id
          update_values[:base_entity_id] = entity_id  # is this needed?
          required_values[name.to_sym] = 1
        end

        if name == 'public_name'

          public_name = n.text
          raise RequestValidationException.new('public_name is too long') if public_name.length > 64

          update_values[:public_name] = n.text
          required_values[name.to_sym] = 1

        end

        if name == 'functional_purpose'
          update_values[:purpose] = n.text
          required_values[name.to_sym] = 1

        end

        if name == 'responsible_dept'
          update_values[:responsible_department] = n.text
          required_values[name.to_sym] = 1
        end

        if name == 'function_owner_dept'
          update_values[:owning_department] = n.text
          required_values[name.to_sym] = 1

        end

        if name == 'audience'
          update_values[:audience] = n.text
          required_values[name.to_sym] = 1
        end

        # don't think this is used
        if name == 'environment'
          update_values[:environment]
          required_values[name.to_sym] = 1
        end

        #  both
        if name == 'new_user_attributes'

          top = true
          n.xpath('new_user_attribute').each do |a|
            logger.info("node #{name} child #{a.name}")
            user = a.xpath('netid').text
            if update_values.has_key?(:users)
              update_values[:users] << user
            else
              update_values[:users] = [user]
              required_values[name.to_sym] = 1
              top = false
            end
          end

          if top
            user = n.xpath('netid').text
            if update_values.has_key?(:users)
              update_values[:users] << user
            else
              update_values[:users] = [user]
              required_values[name.to_sym] = 1
            end
          end
        end

        # both
        if name == 'new_sp_contact_attributes'

          top = true

          n.xpath('new_sp_contact_attribute').each do |a|
            logger.info("node #{name} child #{a.name}")

            contact = {}
            contact[:type] = a.xpath('con_type').text
            contact[:netid] = a.xpath('netid').text
            contact[:email] = a.xpath('email').text
            if update_values.has_key?(:contacts)
              update_values[:contacts] << contact
            else
              update_values[:contacts] = [contact]
              required_values[name.to_sym] = 1
              top = false
            end
          end

          if top
            contact = {}
            contact[:type] = n.xpath('con_type').text
            contact[:netid] = n.xpath('netid').text
            contact[:email] = n.xpath('email').text
            if update_values.has_key?(:contacts)
              update_values[:contacts] << contact
            else
              update_values[:contacts] = [contact]
              required_values[name.to_sym] = 1
            end
          end

        end


        # need to separate attributes from ismemberof attributes an add to structure differently
        # do we need to have public or not attribute or not. I don't think so.
        # both
        if name == 'requested_attributes'

          # if n.name is requested_attribute
          # look through that - loop
          # else it should look like below
          # no need the kids
          top = true

          n.xpath('requested_attribute').each do |a|
            logger.info("node #{name} child #{a.name}")

            attr = {}
            attr[:attr] = a.xpath('attribute_name').text
            attr[:reason] = a.xpath('reason').text
            attr[:value] = a.xpath('value').text

            if update_values.has_key?(:attributes)
              update_values[:attributes] << attr
            else
              update_values[:attributes] = [attr]
              top = false
            end
          end

          if top
            attr = {}
            attr[:attr] = n.xpath('attribute_name').text

            if attr[:attr] == 'isMemberOf'
              grp = {}
              grp[:reason] = n.xpath('reason').text
              grp[:is_value_regex] = n.xpath('is_value_regex').text
              path = n.xpath('value').text
              if (path.nil? || path == '')
                logger.warn('Value required for isMemberOf attribute request')
              else
                grp[:path] = path

                if update_values.has_key?(:groups)
                  update_values[:groups] << grp
                else
                  update_values[:groups] = [grp]
                end
              end
            else # check if populated!

              attr[:reason] = n.xpath('reason').text
              attr[:value] = n.xpath('value').text

              if update_values.has_key?(:attributes)
                update_values[:attributes] << attr
              else
                update_values[:attributes] = [attr]
              end
            end
          end
        end

        # if the group requests arrive separately from isMemberOf
        # not sure how this might appear since this is not the way
        # it's currently used
        if name == 'requested_groups'
          grp = {}
          grp[:is_value_regex] = n.xpath('is_value_regex').text
          path = n.xpath('path').text

          if path.nil? || path == ''
            logger.warn('Path required for group attribute request')
          else

            grp[:path] = path

            if update_values.has_key?(:groups)
              update_values[:groups] << grp
            else
              update_values[:groups] = [grp]
            end
          end
        end

        # this is just the cert?
        # clean carriage returns new lines and begin/end delims
        if name == 'new_pending_sp_attributes'
          cert = n.text
          cert.sub!(/-+BEGIN CERTIFICATE-+[\n\r]+/, '')
          cert.sub!(/[\n\r]*-+END CERTIFICATE-+[\n\r]*/, '')
          cert.strip!
          update_values[:cert] = cert
          required_values[name.to_sym] = 1

        end

        # both
        if name == 'new_pending_sp_assertion_attributes'
          top = true
          n.xpath('new_pending_sp_assertion_attribute').each do |a|
            logger.info("node #{name} child #{a.name}")

            acs = {}
            acs[:location] = a.xpath('url').text
            acs[:binding] = a.xpath('binding').text
            if update_values.has_key?('acs')
              update_values[:acs] << acs
            else
              update_values[:acs] = [acs]
              required_values[name.to_sym] = 1
              top = false
            end
          end

          if top
            acs = {}
            acs[:location] = n.xpath('url').text
            acs[:binding] = n.xpath('binding').text
            if update_values.has_key?('acs')
              update_values[:acs] << acs
            else
              update_values[:acs] = [acs]
              required_values[name.to_sym] = 1
            end
          end

        end

      end

      required_values.each do |key,val|
        if required_values[key] == 0
          logger.info("Missing required field #{key}")
          # need to raise issue
          # keep for later or raise now?
          # raise RequestValidationException.new("Missing required element #{key}")
        end
      end

    rescue Exception => e
      logger.info(e.message)
      p = {result: 'failed validation'}
      render :xml => p, :status => :bad_request, :location => p
      return
    end

    @spreg = SpRegistration.new(update_values[:base_entity_id],is_admin: is_admin)

    p = {}
    results = @spreg.save(update_values, @username)
    if results[:success]
      p = {result: 'success'}
      render :xml => p, :status => :created, :location => p
    else
      p = {result: 'failed'}
      render :xml => p, :status => :internal_server_error, :location => p
    end

  end

  def delete

    # what would that look like?
    # find in sps and see who owns it
    # email when deleted?

  end

  def authtest

    render json: {success: true, username: @username}

  end

  private

  def authenticate_user!
    username = nil
    userpass = nil

    authenticate_or_request_with_http_basic do |header_username, header_password|
      username = header_username
      userpass = header_password
    end

    unless username and userpass
      raise UnauthorizedAction.new("API authentication credentials not present")
    end

    userdn = nil

    #TODO alternative secrets option
    secrets = AuthSecrets::VALUES
    svc_user = secrets['svcdir_bindDn']
    svc_pass = secrets['svcdir_password']
    svc_uri = URI.parse(secrets['svcdir_providerUrl'])

    # hack alert to get around a cert issue without
    # using verify_none
    if (svc_uri.host == 'idms-svcdir-w.oit.duke.edu')
      host = 'ldap.duke.edu'
    else
      host = svc_uri.host
    end

    begin
      ldap = Net::LDAP.new host: host,
                           port: svc_uri.port,
                           encryption: :simple_tls,
                           base: 'dc=duke,dc=edu',
                           auth: {
                               method: :simple,
                               username: svc_user,
                               password: svc_pass
                           }

      unless ldap.bind
        raise UnauthorizedAction.new("API authentication connection failed for user=#{username}")
      end

      authn_group = 'urn:mace:duke.edu:groups:oit:idm:applications:spreg:api'
      search_user = Net::LDAP::Filter.eq("uid", username)
      search_group = Net::LDAP::Filter.eq('isMemberOf', authn_group)
      search_filter = Net::LDAP::Filter.join(search_user, search_group)

      ldap.search(filter: search_filter, attributes: [], return_result: false) {|item|
        userdn = item.dn
      }
    rescue Exception => e
      logger.info(e.message)
      raise UnauthorizedAction.new("API lookup failed for user=#{username}")
    end

    # test user credentials
    begin
      unless Net::LDAP.new(host: 'ldap.duke.edu',
                           port: '636',
                           encryption: :simple_tls,
                           auth: {
                               method: :simple,
                               username: userdn,
                               password: userpass
                           }).bind
        raise UnauthorizedAction.new("API authentication failed for user=#{username}")
      end

    rescue Exception => e
      logger.info(e.message)
      raise UnauthorizedAction.new("API authentication bind failed for user=#{username}")
    end

    @username = username
    logger.info("API authentication success for user=#{username}")
    return true

  end

  def auth_rescue
    render json: {errors: "Authentication failed"}, :status => :unauthorized
  end

  def uniqueEntityId(entity_id)
    # search local sites and incommon

    logger.info("API checking LocalSites registrations for entity id=#{entity_id}")
    if SpRegistration::isEntityIdInLocalSites(entity_id)
      return false
    end

    logger.info("API checking InCommon registrations for entity id=#{entity_id}")
    if SpRegistration::isEntityIdInInCommon(entity_id)
      return false
    end

    return true
  end
end
