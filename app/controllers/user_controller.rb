class UserController < ApplicationController

	def landing
		@eppn = @shib_attrs[:eppn]
		# @saved_sps = SpInProgress::SavedSp.where eppn: @eppn
		@projects = Project.where(sponsor_email: @shib_attrs[:eppn])
		@has_projects = @projects.count != 0 && @projects.count

		netid = @shib_attrs[:uid]
		sp_user = Spreg::User.find_by(netid: netid)
		@sp_user = sp_user
		sp_user ? @sps = sp_user.get_entity_ids : @sp_user = false
	end


	def project_status
		@project = Project.find_by id: params[:id]
		@work_note = WorkNote.new
		@work_notes = @project.work_notes.where(is_public: true)
	end


	#post
	def create_user_note
		@work_note = WorkNote.new(work_note_params(params))
		@work_note.updated_by = @shib_attrs.dig(:display_name)
		@work_note.is_public = true

		if @work_note.save
			flash[:success] = "Work Note Added."
			redirect_back(fallback_location: root_path)
		else
			flash[:error] = "Please enter a work note."
			redirect_back(fallback_location: root_path)
		end
	end


	#get
	def show_sp
		@is_admin = is_admin
		@attributes = Spreg::Attribute.where(available: 1).pluck(:attributeID,:is_public) #attributes to populate drop down
		@sp = SpRegistration.new(params[:entityId].strip)
		authorize_sp_user(@sp)
		@pending ||= Spreg::PendingSp.find_by new_entity_id: params[:entityId]
		@pending ||= Spreg::PendingSp.find_by new_entity_id: "http://#{params[:entityId]}"
		@pending ||= Spreg::PendingSp.find_by new_entity_id: "https://#{params[:entityId]}"


		render "admin/spreg/show"
	end




	private
	def work_note_params(my_params)
		my_params.require(:work_note).permit(:note,:project_id)
	end

end
