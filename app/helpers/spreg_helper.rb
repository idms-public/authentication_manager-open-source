module SpregHelper
  def format_display_certificate(cert)
    cert = cert.scan(/.{1,64}/)
    cert.map! do |c|
      c = "<p class='certline'>" + c + "</p>"
    end

    cert.join
  end
end
