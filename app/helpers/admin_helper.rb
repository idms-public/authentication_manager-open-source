module AdminHelper
	# :nocov:
	
	def color_status(last_update,closed)
		last_update = Time.now.to_i - last_update.to_time.to_i
		unless closed
			if last_update > 2419200 # 4 weeks
				"key-red"
			elsif last_update > 1814400 # 3 weeks
				"key-orange"
			elsif last_update > 1209600 #2 weeks
				"key-blue"
			elsif last_update > 604800 #1 weeks
				"key-green"
			end
		else
			"closed"
		end
	end


	def com_color_status(status)
		case status
			when "In Progress","Integration Requested"
				"key-blue"
			when "Closed/Complete"
				"key-green"
			when "Closed/Incomplete"
				"key-red"
		end
	end
	# :nocov:
	#


	def format_issuer_subject(issuer)
		issuer.split('/').drop(1)
	end

	def attr_collection(attributes)
		results = {}
		attributes.each {|attr,is_public| is_public ? results[attr] = attr : results["#{attr}(private)"] = attr}
		results
	end



end
