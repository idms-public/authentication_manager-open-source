require 'test_helper'

class LdapBrowserTest < ActionDispatch::IntegrationTest
	setup do
		Capybara.current_driver = Capybara.javascript_driver # :selenium by default
		Capybara.current_session.driver.browser.manage.window.maximize
	end


	test 'admin navigation' do
			visit root_path
			header = find("h1#root-header").text
			assert_equal "Authenticating Your Duke Site" , header

			#Become an admin and visit admin layout
			select('danai', from: 'mock_id')
			click_button('Shib')
			visit admin_root_path
			assert find("h2#admin-root-header").text


			### Test that user can filter integration by handler and nav integration menu
			all_rows = all("table#integration-table tbody tr.click-row").count

			click_link('ON HOLD INTEGRATIONS')
			on_hold_rows = all("table#integration-table tbody tr.click-row").count

			click_link('CLOSED INTEGRATIONS')
			closed_rows = all("table#integration-table tbody tr.click-row").count


			assert (all_rows != on_hold_rows) && (on_hold_rows != closed_rows)
			click_link('OPEN INTEGRATIONS')

			click_button('Filter University')
			university_rows = all("table#integration-table tbody tr.click-row").count
			assert all_rows!= university_rows

			click_button('Filter Health')
			health_rows = all("table#integration-table tbody tr.click-row").count
			assert	(all_rows != health_rows) && (health_rows != university_rows)

			click_button('No Filter')
			no_filter_rows = all("table#integration-table tbody tr.click-row").count
			assert all_rows = no_filter_rows


			### Test user can search integrations
			fill_in 'search', with: "a"
			click_button('Search Integrations')
			assert all("table#integration-table tbody tr.click-row").count !=0
	end


	test 'admin spreg' do
		### Become admin and nav to spreg
		visit root_path
		select('danai', from: 'mock_id')
		click_button('Shib')
		visit spreg_path
		assert_equal"SP Registration - Search by Entity ID", find("h2#spreg-header").text


		### test typeahead entityID search is working
		refute page.has_css?('.tt-suggestion')
		fill_in 'entityId', with: JSON.parse(SpRegistration.sps).first[0..2]
		assert page.has_css?('.tt-suggestion')
		typeahead_row = all('.tt-suggestion').first
		typeahead_row.click
		click_on('Search Entity ID')

		### test user is taken to spreg show page
		sub_headers = all('.sp-header').to_a.map {|a| a.text}
		assert sub_headers.include?('ACS')
		assert sub_headers.include?('Info')

		### test that when user clicks edit icon, commit box is shown, and editable fields are also displayed
		refute page.has_css?('#update-sp-container')
		refute page.has_css?('#entity_id')

		all('.sp-edit-icon').first.click
		assert page.has_css?('#update-sp-container')
		assert page.has_css?('#entity_id')
	end

end
