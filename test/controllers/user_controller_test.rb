require 'test_helper'

class UserControllerTest < ActionDispatch::IntegrationTest
	test "get landing" do
		get root_url
		assert_response 200
	end

	test "get project details page" do
		get project_status_url(projects(:one).id)
		assert_response 200
	end

	test "user creates note" do
		post create_user_note_url, params: {work_note: {project_id:projects(:one).id, note:"new note!"}}
		assert_response 302
	end
end
