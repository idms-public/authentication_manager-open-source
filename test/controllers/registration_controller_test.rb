class RegistrationControllerTest < ActionDispatch::IntegrationTest

	test "get new registration path" do
		get register_url
		assert_response :success
	end


	test "create new shib registration" do
		skip("need to be able to set session here")
		post register_create_registration_path, params: {
			project: {name: "hello", purpose: "a purpose", help_details: "need help", sponsor: "test123",sponsor_email: "test@duke.edu",sponsor_duid: "1234567"},
			contacts_attributes: {name: "John Public", netid: "jp124", email: "jp@duke.edu",contact_type: "technical", employeer: "Duke"}
		}, env: {"HTTP_REFERER" => "/test_register"}

		assert_response :found
		assert_redirected_to "/test_register"
		assert_equal "Thank you for for your submission. The Duke Shibboleth Integration Team has been notified of your request.", flash[:success]
	end

	test "do not create incomplete registration" do
	post register_create_registration_path, params: {
		project: { purpose: "a purpose", sponsor: "test123",sponsor_email: "test@duke.edu",sponsor_duid: "1234567"},
		contacts_attributes: {name: "John Public", netid: "jp124", email: "jp@duke.edu",contact_type: "technical", employeer: "Duke"}
	}, env: {"HTTP_REFERER" => "/test_register"}
	assert_response :found
	assert_redirected_to "/test_register"

	assert_equal "Project name must be filled out", flash[:error][:name][1]
	end
end