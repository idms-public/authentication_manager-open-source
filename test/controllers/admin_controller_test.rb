require 'test_helper'

class AdminControllerTest < ActionDispatch::IntegrationTest
		test "mock shib as test admin only access" do
			users = ["admin","john","user"]
			users.each do |i|
				post mock_id_url, params: {mock_id: i},env: {"HTTP_REFERER" => "/"}
				get admin_root_url
				if i == "admin"
				 assert_response 200
			 	else
				 assert_response 401
				end
			end
		end

	test "get closed integrations page"do
		post mock_id_url, params: {mock_id: 'admin'},env: {"HTTP_REFERER" => "/"}
		get admin_closed_request_url
		assert_response 200
	end

	test "get integration show page" do
		post mock_id_url, params: {mock_id: 'admin'},env: {"HTTP_REFERER" => "/"}
		get integration_request_url(projects(:one).id)
		assert_response 200
	end

	test "assign analyst to integration" do
		post mock_id_url, params: {mock_id: 'admin'},env: {"HTTP_REFERER" => "/"}
		post assign_analyst_url(projects(:one).id), params: {status: "test-status",work_note: "a note", employee: "Some Intern"}
		assert_response 302
	end

	test "update integration status" do
		post mock_id_url, params: {mock_id: 'admin'},env: {"HTTP_REFERER" => "/"}
		patch component_status_url(projects(:one).id),params: {status: "test-status",work_note: "a note", employee: "Some Intern"}
		assert_response 302
	end

	test "create a work note for integration" do
		post mock_id_url, params: {mock_id: 'admin'},env: {"HTTP_REFERER" => "/"}
		post create_note_url, params: {work_note: {note:'a note', project_id: 1,is_public: true}}
		assert_response 302

	end


end
