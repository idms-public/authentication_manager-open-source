require 'test_helper'

class WorkNoteTest < ActiveSupport::TestCase
	def setup
		@work_note1 = work_notes(:one)
		@work_note2 = work_notes(:two)
	end

	test "create fresh note" do
		assert note = WorkNote.new(project_id: 1, note: "fresh note", updated_by: "test 123")
		assert note.save
	end

	test "set update status message" do
		assert message = WorkNote.set_message(project: projects(:one),attrs: {status:"test status"})
		assert_equal "Cisco Jabber status changed from Integreation Requested to test status" , message
	end

end
