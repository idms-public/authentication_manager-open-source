class CreatePendingSps < ActiveRecord::Migration[5.1]
  def change
    create_table :pending_sps do |t|
      t.integer :sp_id
      t.integer :user_id
      t.text :certificate
      t.string :new_entity_id
      t.integer :is_delete, :limit => 1
      t.integer :is_api_request, :limit => 1

      t.timestamps null: false
    end
  end
end
