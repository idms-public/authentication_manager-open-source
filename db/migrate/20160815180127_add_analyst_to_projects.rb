class AddAnalystToProjects < ActiveRecord::Migration[5.0]
  def change
    add_column :projects, :analyst, :string
  end
end
