class CreateActivityLogs < ActiveRecord::Migration[5.1]
  def change
    create_table :activity_logs do |t|
      t.string :level
      t.string :netid
      t.string :activity
      t.string :description

      t.timestamps null: false
    end
  end
end
