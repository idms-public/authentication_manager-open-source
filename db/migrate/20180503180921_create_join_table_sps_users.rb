class CreateJoinTableSpsUsers < ActiveRecord::Migration[5.1]
  def change
    create_join_table :sps, :users do |t|
      # t.index [:sp_id, :user_id]
      # t.index [:user_id, :sp_id]
    end
  end
end
