class CreateJoinTableOwnersSps < ActiveRecord::Migration[5.1]
  def change
    create_join_table :owners, :sps do |t|
      # t.index [:owner_id, :sp_id]
      # t.index [:sp_id, :owner_id]
    end
  end
end
