class CreateLocalSites < ActiveRecord::Migration[5.1]
  def change
    create_table :local_sites do |t|
      t.string :entity_id
      t.integer :registered, :limit => 1

      t.timestamps null: false
    end
  end
end
