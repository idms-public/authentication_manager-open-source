class CreateOwners < ActiveRecord::Migration[5.1]
  def change
    create_table :owners do |t|
      t.text :owner
      t.string :owner_type
      t.timestamps null: false

    end
  end
end
