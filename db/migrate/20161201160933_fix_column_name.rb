class FixColumnName < ActiveRecord::Migration[5.0]
  def change
    rename_column :projects, :considerations, :help_details
  end
end
