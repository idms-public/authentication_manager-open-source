class CreatePendingSpAttrs < ActiveRecord::Migration[5.1]
  def change
    create_table :pending_sp_attrs do |t|
      t.integer :sp_id
      t.integer :attribute_id
      t.string :value
      t.integer :is_value_regex, :limit => 1
      t.text :reason
      t.integer :is_approved, :limit => 1
      t.integer :is_delete, :limit => 1

      t.timestamps null: false
    end
  end
end
