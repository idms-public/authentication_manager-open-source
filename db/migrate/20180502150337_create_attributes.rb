class CreateAttributes < ActiveRecord::Migration[5.1]
  def change
    create_table :attributes do |t|
      t.string :attributeID
      t.integer :is_public, :limit => 1
      t.integer :values_optional, :limit => 1
      t.integer :available, :limit => 1
      
      t.timestamps null: false
    end
  end
end
