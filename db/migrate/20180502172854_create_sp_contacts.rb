class CreateSpContacts < ActiveRecord::Migration[5.1]
  def change
    create_table :sp_contacts do |t|
      t.integer :sp_id
      t.string :con_type
      t.string :netid
      t.string :email
      
      t.timestamps null: false
    end
  end
end
