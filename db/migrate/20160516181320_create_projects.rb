class CreateProjects < ActiveRecord::Migration[4.2]
  def change
    create_table :projects do |t|
      t.string :name
      t.string :sponsor
      t.string :sponsor_duid
      t.string :sponsor_email
      t.string :status,:default => "Integration Requested"
      t.text :considerations
      t.text :purpose
      t.string :updated_by

      t.timestamps null: false
    end
  end
end
