class AddComponentAttrsToProjects < ActiveRecord::Migration[5.0]
  def change
    add_column :projects, :prod_url, :string
    add_column :projects, :test_url, :string
    add_column :projects, :webserver, :string
    add_column :projects, :os, :string
    # add_column :projects, :status, :string, :default => "Integration Requested"
    add_column :projects, :is_saml_supported, :string
    add_column :projects, :non_web_desc, :text
    add_column :projects, :is_vendor, :boolean
    add_column :projects, :is_web_based, :boolean
  end
end
