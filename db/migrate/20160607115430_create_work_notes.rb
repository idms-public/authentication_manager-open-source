class CreateWorkNotes < ActiveRecord::Migration[4.2]
  def change
    create_table :work_notes do |t|
      t.integer :project_id
      t.integer :component_id
      t.text :note
      t.string :updated_by
      t.timestamps null: false
    end
  end
end
