class CreateSps < ActiveRecord::Migration[5.1]
  def change
    create_table :sps do |t|
      t.string :entity_id
      t.string :public_name
      t.text :functional_purpose
      t.text :responsible_dept
      t.text :function_owner_dept
      t.text :audience
      t.integer :is_draft, :limit => 1
      t.timestamps null: true
      t.string :url
      t.integer :is_api_request, :limit => 1
      t.string :environment

    end
  end
end
