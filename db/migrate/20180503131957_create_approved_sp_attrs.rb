class CreateApprovedSpAttrs < ActiveRecord::Migration[5.1]
  def change
    create_table :approved_sp_attrs do |t|
      t.integer :sp_id
      t.integer :attribute_id
      t.string :value
      t.integer :is_value_regex, :limit => 1
      t.text :reason
      t.string :approved_id

      t.timestamps null: false
    end
  end
end
