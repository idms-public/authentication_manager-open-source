class CreatePendingSpAssertions < ActiveRecord::Migration[5.1]
  def change
    create_table :pending_sp_assertions do |t|
      t.integer :sp_id
      t.string :index
      t.string :binding
      t.text :url
      t.integer :is_delete, :limit => 1
      t.integer :is_default, :limit => 1

      t.timestamps null: false

    end
  end
end
