class AddInstitutionToProjects < ActiveRecord::Migration[5.1]
  def change
    add_column :projects, :institution, :string
  end
end
