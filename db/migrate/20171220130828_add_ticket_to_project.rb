class AddTicketToProject < ActiveRecord::Migration[5.1]
  def change
    add_column :projects, :ticket, :string
  end
end
