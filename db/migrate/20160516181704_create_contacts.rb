class CreateContacts < ActiveRecord::Migration[4.2]
  def change
    create_table :contacts do |t|
      t.integer :component_id
      t.integer :project_id
      t.string :name
      t.string :netid
      t.string :email
      t.string :contact_type
      t.string :employer
      t.string :updated_by


      t.timestamps null: false
    end
  end
end
