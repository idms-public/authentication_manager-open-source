class AddIsPublicToWorkNotes < ActiveRecord::Migration[5.0]
  def change
    add_column :work_notes, :is_public, :boolean
  end
end
