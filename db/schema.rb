# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20180503180921) do

  create_table "activity_logs", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=latin1" do |t|
    t.string "level"
    t.string "netid"
    t.string "activity"
    t.string "description"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "approved_sp_attrs", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=latin1" do |t|
    t.integer "sp_id"
    t.integer "attribute_id"
    t.string "value"
    t.integer "is_value_regex", limit: 1
    t.text "reason"
    t.string "approved_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "attributes", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=latin1" do |t|
    t.string "attributeID"
    t.integer "is_public", limit: 1
    t.integer "values_optional", limit: 1
    t.integer "available", limit: 1
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "contacts", id: :integer, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=latin1" do |t|
    t.integer "component_id"
    t.integer "project_id"
    t.string "name"
    t.string "netid"
    t.string "email"
    t.string "contact_type"
    t.string "employer"
    t.string "updated_by"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "local_sites", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=latin1" do |t|
    t.string "entity_id"
    t.integer "registered", limit: 1
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "owners", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=latin1" do |t|
    t.text "owner"
    t.string "owner_type"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "owners_sps", id: false, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=latin1" do |t|
    t.bigint "owner_id", null: false
    t.bigint "sp_id", null: false
  end

  create_table "pending_sp_assertions", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=latin1" do |t|
    t.integer "sp_id"
    t.string "index"
    t.string "binding"
    t.text "url"
    t.integer "is_delete", limit: 1
    t.integer "is_default", limit: 1
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "pending_sp_attrs", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=latin1" do |t|
    t.integer "sp_id"
    t.integer "attribute_id"
    t.string "value"
    t.integer "is_value_regex", limit: 1
    t.text "reason"
    t.integer "is_approved", limit: 1
    t.integer "is_delete", limit: 1
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "pending_sps", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=latin1" do |t|
    t.integer "sp_id"
    t.integer "user_id"
    t.text "certificate"
    t.string "new_entity_id"
    t.integer "is_delete", limit: 1
    t.integer "is_api_request", limit: 1
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "projects", id: :integer, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=latin1" do |t|
    t.string "name"
    t.string "sponsor"
    t.string "sponsor_duid"
    t.string "sponsor_email"
    t.string "status", default: "Integration Requested"
    t.text "help_details"
    t.text "purpose"
    t.string "updated_by"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "analyst"
    t.string "prod_url"
    t.string "test_url"
    t.string "webserver"
    t.string "os"
    t.string "is_saml_supported"
    t.text "non_web_desc"
    t.boolean "is_vendor"
    t.boolean "is_web_based"
    t.string "institution"
    t.string "ticket"
  end

  create_table "sp_contacts", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=latin1" do |t|
    t.integer "sp_id"
    t.string "con_type"
    t.string "netid"
    t.string "email"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "sps", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=latin1" do |t|
    t.string "entity_id"
    t.string "public_name"
    t.text "functional_purpose"
    t.text "responsible_dept"
    t.text "function_owner_dept"
    t.text "audience"
    t.integer "is_draft", limit: 1
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string "url"
    t.integer "is_api_request", limit: 1
    t.string "environment"
  end

  create_table "sps_users", id: false, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=latin1" do |t|
    t.bigint "sp_id", null: false
    t.bigint "user_id", null: false
  end

  create_table "users", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=latin1" do |t|
    t.string "netid"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "work_notes", id: :integer, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=latin1" do |t|
    t.integer "project_id"
    t.integer "component_id"
    t.text "note"
    t.string "updated_by"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.boolean "is_public"
  end

end
