### Populate Mock Projects for Shibboleth Integration Requests ###
role = ["functional","technical"]
webserver = ["apache","nginx","other"]
institution = ["University", "Health System"]
os=["windows","mac","linux"]
rand_bool = [true,false]
statuses = ["In Progress","Closed/Complete","Closed/Incomplete","Integration Requested"]


20.times do
	app = Faker::App.name
	web_based = rand_bool.sample
	project = Project.create(
		name: app,
		sponsor_duid:"1234567",
		sponsor_email:"email@duke.edu",
		help_details:Faker::Lorem.sentence,purpose:Faker::Lorem.sentence,
	  sponsor: Faker::WorldOfWarcraft.hero,
		prod_url:"#{app}.com",test_url: "test-#{app}.com",
		webserver: webserver.sample,
		is_saml_supported:rand_bool.sample,
		os: os.sample,
		is_web_based: web_based,
		is_vendor: rand_bool.sample,
		status:statuses.sample,
		institution: institution.sample,
		non_web_desc: web_based ? nil : Faker::Lorem.sentence
	)

	Contact.create(
		project_id:project.id,
		name: Faker::WorldOfWarcraft.hero,
		netid:"abc123",
		email: Faker::Internet.email,
		contact_type:role.sample,
		employer: Faker::Company.name
	)
end
######


### Populate Sp Table ###
#TODO give option to specify number of generated_sps 
generated_sps = []
20.times do
	generated_sps << Spreg::Sp.create(
		entity_id: Faker::Internet.url,
		public_name: Faker::App.name,
		functional_purpose: Faker::Lorem.sentence,
		responsible_dept: "IT",
		function_owner_dept: "History",
		audience: "students",
		environment: "production"
	)
end
######


### Populate User Table ###
#TODO give config option to specify mock use_mattermost
users = []
users << Spreg::User.create(netid: "netid-user-one")
users << Spreg::User.create(netid: "netid-user-two")
users << Spreg::User.create(netid: "netid-user-three")
users << Spreg::User.create(netid: "da129")
######


### Randomly assign mock user to mock sp ###
generated_sps.each do |sp|
	sp.users << users.sample
	sp.save
end
######


### Populate Attribute Table ###
attributes = ['displayName','duDukeID','duMiddleName','duProxyToken','duPSEmplID',
	'duSAPActiveStafferPin','duSAPCompany','duSAPNameFirst','duSAPNameLast',
	'duSAPOrgUnit','duSAPPayrollArea','duSAPTimePCPin','duSMSMobile','eduCourseMember',
	'eduPersonAffiliation','eduPersonEntitlement','eduPersonNickname','eduPersonPrimaryAffiliation',
	'eduPersonPrincipalName','eduPersonScopedAffiliation','givenName','isMemberOf',
	'mail','ou','sn','telephoneNumber','title','transientId','uid','surname','cn',
	'nameid-eppn','nameid-uid','nameid-eppn-email','givenName','mail','ou','sn','telephoneNumber',
	'isMemberOf','title','uid','surname','cn']
attributes.each do |attr|
	Spreg::Attribute.create(attributeID: attr, is_public: 1, available: 1)
end
######


### Generate Mock IDP files ###

#Create local-sites.xml
xml_string = """<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\"?>
<EntitiesDescriptor xmlns=\"urn:oasis:names:tc:SAML:2.0:metadata\" xmlns:ds=\"http://www.w3.org/2000/09/xmldsig#\" xmlns:shibmd=\"urn:mace:shibboleth:metadata:1.0\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" Name=\"urn:mace:duke.edu:shib:federation\" validUntil=\"2030-01-01T00:00:00Z\" xsi:schemaLocation=\"urn:oasis:names:tc:SAML:2.0:metadata ../schemas/saml-schema-metadata-2.0.xsd urn:mace:shibboleth:metadata:1.0 ../schemas/shibboleth-metadata-1.0.xsd http://www.w3.org/2000/09/xmldsig# ../schemas/xmldsig-core-schema.xsd\">

</EntitiesDescriptor>
"""
File.write('vendor/shibboleth/metadata/local-sites.xml', xml_string)


doc = File.open("vendor/shibboleth/metadata/local-sites.xml") { |f| Nokogiri::XML(f) }
ed = doc.search("EntitiesDescriptor").first

#add entries to local-sites.xml
generated_sps.each do |sp|
	new_xml = <<-XML
	<EntityDescriptor entityID="#{sp.entity_id}">
	<SPSSODescriptor AuthnRequestsSigned="0" protocolSupportEnumeration="urn:oasis:names:tc:SAML:2.0:protocol urn:oasis:names:tc:SAML:1.1:protocol urn:oasis:names:tc:SAML:1.0:protocol http://schemas.xmlsoap.org/ws/2003/07/secext">
		<KeyDescriptor>
			<ds:KeyInfo>
				<ds:X509Data>
					<ds:X509Certificate>
MIIEpjCCA46gAwIBAgIBADANBgkqhkiG9w0BAQsFADCBlzELMAkGA1UEBhMCVVMx
FzAVBgNVBAgMDk5vcnRoIENhcm9saW5hMQ8wDQYDVQQHDAZEdXJoYW0xGDAWBgNV
BAoMD0R1a2UgVW5pdmVyc2l0eTEMMAoGA1UECwwDT0lUMRcwFQYDVQQDDA5zYW1s
Q2FtZWwgdGVzdDEdMBsGCSqGSIb3DQEJARYOZGExMjlAZHVrZS5lZHUwHhcNMTgw
NDI2MTIyMjI3WhcNMTkwNDI2MTIyMjI3WjCBlzELMAkGA1UEBhMCVVMxFzAVBgNV
BAgMDk5vcnRoIENhcm9saW5hMQ8wDQYDVQQHDAZEdXJoYW0xGDAWBgNVBAoMD0R1
a2UgVW5pdmVyc2l0eTEMMAoGA1UECwwDT0lUMRcwFQYDVQQDDA5zYW1sQ2FtZWwg
dGVzdDEdMBsGCSqGSIb3DQEJARYOZGExMjlAZHVrZS5lZHUwggEiMA0GCSqGSIb3
DQEBAQUAA4IBDwAwggEKAoIBAQDNCM6/9GJCxuGwVnQdFT9m5AGfsvSNfLz35FdK
xt4Aj7OO6MSgZxa5XRWqhSRVY5U33jGWH2NSbBse2BBo8AvnfQDJzpF8yVKzHPqG
53+h62yvtsftOThDxaluBTRThVrJ5S4U4+DZZbbfk1CaMaSWrY68GFEeeP1Q5wuQ
zc/8KpFRJWympFEDQiu9mZ3Fd1AkS2/Uj55fa9bfFQdHBawIAEr8sGwh7n0JX5ZF
pBuR/Bg/PZ1dNER53hG+hNthQK1K6PDzPy318Ek/5Gc+dXKlBs16DpUqbzrOEqyD
6FTtL94Tql61g55NHw3z+p67jsIysDqvhaqxeXWyJGVXxBSnAgMBAAGjgfowgfcw
DwYDVR0TAQH/BAUwAwEB/zAdBgNVHQ4EFgQU+Dvbi+lExegFlqadQWS2GK4ihIkw
gcQGA1UdIwSBvDCBuYAU+Dvbi+lExegFlqadQWS2GK4ihImhgZ2kgZowgZcxCzAJ
BgNVBAYTAlVTMRcwFQYDVQQIDA5Ob3J0aCBDYXJvbGluYTEPMA0GA1UEBwwGRHVy
aGFtMRgwFgYDVQQKDA9EdWtlIFVuaXZlcnNpdHkxDDAKBgNVBAsMA09JVDEXMBUG
A1UEAwwOc2FtbENhbWVsIHRlc3QxHTAbBgkqhkiG9w0BCQEWDmRhMTI5QGR1a2Uu
ZWR1ggEAMA0GCSqGSIb3DQEBCwUAA4IBAQAoS+ROABs6ckP/iLHD6fpQt4it05pQ
7iFnZNFXSOLRO72nN58PprQwq2nitWT2nHI6JZ8nFhXTnaJ/vz9N9AKCeuIr3AQ0
dZXPMTwnl/JNSEbcoEGlBhNHmMEYdHs6GuJSMNdbaQ6CBt7UvxJt2znc6+Hly8EB
CGwfXJDqzWnjJBX4tThRw1CXHm7UynN6g27k7q91Hea4Ij3zdCc1PdDqXVafDm3Q
zbgkpWAhf8g5SrD7nlDyrD12sh8mMto5O838Q/J4TSQvmwx3HAQcrB/VNL4T68CE
r/nJZR+vJ7WXpNItz3A2IalBtz2YRcXerBOwbS6bCjo0iE7mMn0iX+w/
					</ds:X509Certificate>
				</ds:X509Data>
			</ds:KeyInfo>
		</KeyDescriptor>
		<NameIDFormat>urn:mace:shibboleth:1.0:nameIdentifier</NameIDFormat>
		<NameIDFormat>urn:oasis:names:tc:SAML:2.0:nameid-format:transient</NameIDFormat>
		<AssertionConsumerService Binding="urn:oasis:names:tc:SAML:2.0:bindings:HTTP-POST" Location="#{sp.entity_id}/Shibboleth.sso/SAML2/POST" index="1" isDefault="true"/>
	</SPSSODescriptor>
	</EntityDescriptor>
	XML
	ed.add_child(new_xml)
end
File.write('vendor/shibboleth/metadata/local-sites.xml', doc.to_xml)


#create attribute-filter.xmlns
xml_string = <<-XML
<?xml version="1.0" encoding="UTF-8" standalone="no"?>
<AttributeFilterPolicyGroup xmlns="urn:mace:shibboleth:2.0:afp" xmlns:basic="urn:mace:shibboleth:2.0:afp:mf:basic" xmlns:saml="urn:mace:shibboleth:2.0:afp:mf:saml" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" id="ShibbolethFilterPolicy" xsi:schemaLocation="urn:mace:shibboleth:2.0:afp classpath:/schema/shibboleth-2.0-afp.xsd                         urn:mace:shibboleth:2.0:afp:mf:basic classpath:/schema/shibboleth-2.0-afp-mf-basic.xsd                         urn:mace:shibboleth:2.0:afp:mf:saml classpath:/schema/shibboleth-2.0-afp-mf-saml.xsd">

</AttributeFilterPolicyGroup>
XML
File.write('vendor/shibboleth/conf/attribute-filter.xml', xml_string)

#add entries to attribute-filter.xmlns
doc = File.open("vendor/shibboleth/conf/attribute-filter.xml") { |f| Nokogiri::XML(f) }
ed = doc.search("AttributeFilterPolicyGroup").first
generated_sps.each do |sp|
	new_xml = <<-XML
	<AttributeFilterPolicy id="#{sp.entity_id}">
	<PolicyRequirementRule value="#{sp.entity_id}" xsi:type="basic:AttributeRequesterString"/>
	<AttributeRule attributeID="uid">
		<PermitValueRule xsi:type="basic:ANY"/>
	</AttributeRule>
	<AttributeRule attributeID="mail">
		<PermitValueRule xsi:type="basic:ANY"/>
	</AttributeRule>
	<AttributeRule attributeID="sn">
		<PermitValueRule xsi:type="basic:ANY"/>
	</AttributeRule>
	<AttributeRule attributeID="eduPersonPrimaryAffiliation">
		<PermitValueRule xsi:type="basic:ANY"/>
	</AttributeRule>
	<AttributeRule attributeID="isMemberOf">
		<PermitValueRule xsi:type="basic:OR">
		<basic:Rule value="urn:mace:duke.edu:groups:oit:csi:is:idmtech" xsi:type="basic:AttributeValueString"/>
		<basic:Rule regex="urn:mace:duke.edu:groups:oit:idm:applications:affiliates:.*" xsi:type="basic:AttributeValueRegex"/>
		<basic:Rule regex="urn:mace:duke.edu:groups:oit:idm:applications:onelink:.*" xsi:type="basic:AttributeValueRegex"/>
		<basic:Rule value="urn:mace:duke.edu:groups:dhts:dhead:dukemedicalmembers" xsi:type="basic:AttributeValueString"/>
		<basic:Rule regex="urn:mace:duke.edu:groups:oit:idm:applications:identity-manager:.*" xsi:type="basic:AttributeValueRegex"/>
		</PermitValueRule>
	</AttributeRule>
	<AttributeRule attributeID="givenName">
		<PermitValueRule xsi:type="basic:ANY"/>
	</AttributeRule>
	</AttributeFilterPolicy>
	XML
	ed.add_child(new_xml)
end
File.write('vendor/shibboleth/conf/attribute-filter.xml', doc.to_xml)
######
